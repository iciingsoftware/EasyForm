﻿#include "ocv.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
#include <QDir>
#include <iostream>
using namespace std;

OCV::OCV(QImage imagen)
{
    this->imagen = QImageToCvMat(imagen);
    cantidadRespuestas = 0;
}

OCV::OCV(Mat imagen)
{
    this->imagen = imagen;
}

OCV::~OCV()
{
    //imagen.deallocate();// espero q elimine la imagen
}

QImage OCV::BuscarRespuestas(QImage imagen, Tipo_Respuesta scope)
{

    /*
     * buscar respuestas es la funcion publica de la clase que se encarga de
     * llamar a las funciones privadas con el fin de encontrar las respuestas marcadas
     * */
    Mat cvMat = QImageToCvMat(imagen);
    Mat filtrada;
    Mat filtradaCopy;
    QList <Rect> imHoriz, imVert;

    if (scope == IDENTIFICACION)
    {
        Mat cpyCvMat (cvMat);
        rotate(cpyCvMat, 90.0, cvMat);
        cpyCvMat.release();
    }

    imDesarrollo = cvMat.clone();
    filtrada = aplicarFiltros(cvMat,false);
    /*
     * se clona la imagen puesto que mas adelante se utiliza la funcion
     * find contours la que modifica la imagen, haciendola inutilizable
     * */
    filtradaCopy = filtrada.clone();
    //se segmenta en filas
    imHoriz = segmentacionProjectionBased(filtrada,
                                          HORIZONTAL);
    //se segmenta en columnas
    imVert = segmentacionProjectionBased(filtrada,
                                         VERTICAL);


    QList <QList<QRect> > respuestasRoi;

    if (scope == ALTERNATIVA)
    {
        QList<QList<QRect> > segCols;
        segCols = segmentacionColumnas(filtrada, imVert, imHoriz);
        //se segmentan los cuadros de respuestas y se guardan como regiones de interes
        respuestasRoi = segmentarRespuestas(segCols, imHoriz);
        //se identifica cual es o son las respuestas marcadas de las preguntas
        resp = identificarRespuestas(filtradaCopy, respuestasRoi);
        print(imDesarrollo, imVert, Scalar(255,0,0));
        print(imDesarrollo, imHoriz, Scalar(0,0,255));

    }
    else if (scope == IDENTIFICACION)
    {
        respuestasRoi = segmentarRespuestas(imVert, imHoriz);
        idResp = identificarRespuestas(filtradaCopy, respuestasRoi);

        print(imDesarrollo, imVert, Scalar(255,0,0));
        print(imDesarrollo, imHoriz, Scalar(0,0,255));
    }

    filtrada.release();
    /*
    // se dibujan los cuadros de respuestas, solo para depuracion
    for (int i = 0; i < respuestasRoi.size(); i++)
    {
        QList <QRect> temp = respuestasRoi [i];
        print(cvMat, temp,Scalar (0,0,255),i);
    }
    */
    for (int i = 0; i < resp.size(); i++)
    {
        if (resp.at(i) <0)
            continue;
        Rect r = QRect2Rect(respuestasRoi.at(i).at(resp.at(i)));
        rectangle(cvMat, r,
                  Scalar (0,0,255));
    }
    return cvMatToQImage(cvMat);
}

std::vector<std::vector<Point> > OCV::buscarRectangulos()
{
    vector<vector<Point> > contornos; //almacena los contornos
    vector<Point> hull;
    Mat gray0;
    Mat resized;

    // se escala la imagen a la mitad, espero que esto haga mas rapido al
    // algoritmo, se supone que escalar la imagen es un proceso lento
    resize (imagen,resized, Size(),
            0.5,0.5, CV_INTER_AREA);
    gray0 = aplicarFiltros(resized);
    Mat temp;
    cvtColor(gray0,temp, CV_GRAY2BGR);
    QImage im = cvMatToQImage(temp);
    im.save("holap.jpg");
    findContours(gray0,contornos,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE,Point (3,3));

    int  areaHoja = gray0.cols * gray0.rows;

    //encuentra los contornos y los almacena en una matriz

    for (uint i=0; i< contornos.size();i++)
    {
        Rect boundRect = boundingRect(contornos[i]);
        // encuentra un rectangulo que encapsula a todo el contorno
        double areaContorno,areaBounding;
        areaBounding = boundRect.area();
        areaContorno = fabs(contourArea(contornos[i]));

        //comparamos el area del rectangulo que encapsula y del
        //contorno. Si son similares, podemos decir que el contorno
        //tiene una forma similar a un rectangulo aunque tenga bordes
        //redondos y lo agregamos a la lista de contornos utiles

        if (areaContorno / areaBounding > 0.85 and areaContorno / areaHoja > 0.003)
        {
            convexHull(contornos[i], hull);
            cuadrados.push_back(hull);
        }

    }
    /*
     * como la imagen se escaló a la mitad, necesitamos
     * escalar los contornos para compensar
     * */
    for (uint i = 0; i < cuadrados.size(); i++)
    {
        for (uint j = 0; j < cuadrados[i].size(); j++)
        {
            cuadrados[i][j].x *= 2;
            cuadrados[i][j].y *= 2;
        }
    }

    resized.release();
    return cuadrados;
}

std::vector<std::vector<Point> > OCV::getCuadrados()
{
    return cuadrados;
}

//convertir de formato de imagen de Qt a formato de imagen de Opencv
cv::Mat OCV::QImageToCvMat(const QImage &inImage, bool inCloneImageData)
{
    switch ( inImage.format() )
         {
            // 8-bit, 4 channel
            case QImage::Format_RGB32:
            {
               cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC4, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

               return (inCloneImageData ? mat.clone() : mat);
            }

            // 8-bit, 3 channel
            case QImage::Format_RGB888:
            {

               QImage   swapped = inImage.rgbSwapped();

               return cv::Mat( swapped.height(), swapped.width(), CV_8UC3, const_cast<uchar*>(swapped.bits()), swapped.bytesPerLine() ).clone();
            }

            // 8-bit, 1 channel
            case QImage::Format_Indexed8:
            {
               cv::Mat  mat( inImage.height(), inImage.width(), CV_8UC1, const_cast<uchar*>(inImage.bits()), inImage.bytesPerLine() );

               return (inCloneImageData ? mat.clone() : mat);
            }

            default:

               break;
         }

    return cv::Mat();
}

//convertir de imagen de OpenCv a Qt
QImage OCV::cvMatToQImage(const cv::Mat &inMat)
{
    Mat clon;
    clon = inMat.clone();
    switch ( inMat.type() )
         {
            // 8-bit, 4 channel
            case CV_8UC4:
            {
               QImage image( clon.data, clon.cols, clon.rows, clon.step, QImage::Format_RGB32 );
               QImage image2 (image);

               image2.detach();
               return image2;
            }

            // 8-bit, 3 channel
            case CV_8UC3:
            {
               QImage image( clon.data, clon.cols, clon.rows, clon.step, QImage::Format_RGB888 );
               QImage image2 (image.rgbSwapped());

               image2.detach();
               return image2;
            }

            // 8-bit, 1 channel
            case CV_8UC1:
            {
               static QVector<QRgb>  sColorTable;

               // only create our color table once
               if ( sColorTable.isEmpty() )
               {
                  for ( int i = 0; i < 256; ++i )
                     sColorTable.push_back( qRgb( i, i, i ) );
               }

               QImage image( clon.data, clon.cols, clon.rows, clon.step, QImage::Format_Indexed8 );

               image.setColorTable( sColorTable );

               return image;
            }

            default:

               break;
         }

    return QImage();
}

Mat OCV::aplicarFiltros(Mat imagen, bool applyErode)
{
    Mat gray;
    cvtColor(imagen, gray, CV_BGR2GRAY);// se convierte la imagen a escala de grises

    //bilateralFilter(gray,gray0,2,10,5); // por ahora se usara un bilateral blur con el
    //fin de reducir el ruido y a la vez cuidar los contornos, el problema es que
    //es MUY lento, candidato a no estar



    //GaussianBlur(gray,gray,Size (5,5),0);
    //hace mas borrosa la imagen para reducir el ruido
    //morphologyEx(gray,gray,MORPH_OPEN,(3,3),Point(1,1),6);
    erode(gray,gray,Mat());
    dilate (gray,gray, Mat());
    if (applyErode)
        erode (gray,gray, Mat());
    //equalizeHist(gray,gray);
    gray = levelFilter(gray,200, 255, 5);
   /*
    Mat tmp;
    cvtColor(gray,tmp,CV_GRAY2BGR);
    QImage im = cvMatToQImage(tmp);
    im.save("preThresh.png");
    */
    threshold(gray,gray, 200, 255,THRESH_BINARY_INV);
    //threshold(gray,gray,0,255, CV_THRESH_BINARY|CV_THRESH_OTSU);



    //dilate(gray,gray,3);
    /*Mat gray0;
    Canny (gray,gray0,50,150);
    */
    //canny es un operador que filtra bordes de acuerdo
    // a 2 operaciones de threshold

    return gray;
}

double OCV::angulo(Point p1, Point p2, Point p0)
{
    // lo saque de internet, ni me moleste en entender
    double dx1 = p1.x - p0.x;
    double dy1 = p1.y - p0.y;
    double dx2 = p2.x - p0.x;
    double dy2 = p2.y - p0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

Mat OCV::levelFilter(Mat im, int inNegro, int inBlanco, double gamma)
{
    /*
     * el filtro niveles es un filtro que se encuentra en muchos programas de
     * edicion de imagenes y puede usarse para aumentar el contraste de la imagen
     * la formula se obutvo desde
     * http://http.developer.nvidia.com/GPUGems/gpugems_ch22.html
     * */
    gamma = 1 / gamma;
    Mat res;
    Mat lut_matrix (1,256,CV_8UC1);

    uchar* data = lut_matrix.data;
    for (int i=0; i < 256; i++)
    {
        double pre = (double) (i - inNegro) / (inBlanco - inNegro);
        double res = pow (pre, gamma) * 255.0;
        data[i] = res;
    }
    // lut es, segun la documentacion de opencv, la forma mas rapida de editar
    // una imagen basandose en una lookup table
    LUT(im, lut_matrix, res);
    return res;
}

/*
 * set label es una funcion de ayuda usada durante la fase de implementacion
 * para escribir texto en el centro de un contorno. Obtenida desde
 * https://github.com/bsdnoobz/opencv-code/blob/master/shape-detect.cpp
 * */
void OCV::setLabel(Mat &im, const string label, std::vector<Point> &contour)
{
    int fontface = cv::FONT_HERSHEY_SIMPLEX;
    double scale = 0.4;
    int thickness = 1;
    int baseline = 0;

    cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);
    cv::Rect r = cv::boundingRect(contour);

    cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
    cv::rectangle(im, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255,255,255), CV_FILLED);
    cv::putText(im, label, pt, fontface, scale, CV_RGB(0,0,0), thickness, 8);
}

void OCV::setLabel(Mat &im, const string label, Rect r)
{
    int fontface = cv::FONT_HERSHEY_SIMPLEX;
    double scale = 0.4;
    int thickness = 1;
    int baseline = 0;

    cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);

    cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
    cv::rectangle(im, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255,255,255), CV_FILLED);
    cv::putText(im, label, pt, fontface, scale, CV_RGB(0,0,0), thickness, 8);
}

QList<int> OCV::getRespuestas()
{
    return resp;
}

QString OCV::getIdentificacionAlumno(bool bFormatoRut)
{
    QString identificacion;
    for (int i = idResp.size() - 1; i >= 0; i--)
    {
        int n1 = idResp.size() - 1 - i;

        if (idResp.at(i) < 0 and i != 0)
            return QString();

        if (i == 0)
            identificacion.append("-");
        else if ((idResp.size()  - i)%3 == 0)
            identificacion.append(".");


        if (idResp.at(i) == 10 or idResp.at(i) == -1)
            identificacion.append("k");
        else
            identificacion.append(QString::number(idResp.at(i)));
    }
    return identificacion;
}

void OCV::rotate(Mat &src, double angle, Mat &dst)
{
    int len = std::max(src.cols, src.rows);
    cv::Point2f pt(len/2., len/2.);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);

    cv::warpAffine(src, dst, r, cv::Size(len, len));
}



QVector<int> OCV::pixHorizontalProj(Mat im)
{
    /*
     * esta funcion recorre la imagen contando cuantos pixeles blancos hay
     * en cada fila de la imagen (recordar que la imagen se encuentra invertida)
     *  y se almacena en un vector. El vector tiene
     * el mismo largo q las filas de la imagen.
     * Esto se le conoce como proyeccion horizontal y sera utilizada como medida
     * estadistica para determinar la hubicacion de los cuadros de respuestas y
     * su posterior segmentacion.
     * */
    QVector<int> proyeccion(im.rows);
    int cont;
    uchar *ptr;

    int prom = 0;

    for (int i=0; i < im.rows; i++)
    {
        ptr = im.ptr(i); // obtiene una fila de la imagen
        cont = 0;
        for (int j=0; j < im.cols; j++)
        {
            if (ptr[j] > 0)
                cont++; // si el pixel no es negro, aumenta el contador
        }
        // si la fila es casi completamente blanca, entonces se asume que es
        //un elemento no deseado y se filtra asignandole el valor 0
        if ((float) cont / im.cols > 0.95)
            cont = 0;
        proyeccion[i] = cont; // se guardan la cantidad de pixeles en el vector
        prom += cont;
    }
    prom /= im.rows;
    prom /= 2;

    Mat proy (im.rows, im.cols, CV_8UC3, Scalar (255,255,255));
    for (int i = 0; i < im.rows; i++)
        line(proy, Point(0,i), Point (proyeccion.at(i), i), Scalar (0,0,0));

    line(proy, Point (prom,0), Point (prom, im.rows),
         Scalar(0,0,255));
    QImage imagen = cvMatToQImage(proy);
    imagen.save("horProj.png");

    return proyeccion;
}

QVector<int> OCV::pixVerticalProj(Mat im)
{
    /*
     * lo mismo que pixHorizontalProj pero ahora por columnas
     * */
    QVector<int> proj(im.cols);
    int ancho = im.cols;
    int sum;
    uchar *data;

    data = im.data;
    int i,j;
    int prom = 0;

    for (i = 0; i < im.cols; i++)
    {
        sum = 0;
        for (j = 0; j < im.rows; j++)
        {
            if (data[i + j * ancho] > 0)
                sum++;
        }
        if ((float)sum / im.rows > 0.95)
            sum = 0;
        proj[i] = sum;
        prom += sum;
    }
    prom /= im.cols;
    prom /= 2;

    Mat proy (im.rows, im.cols, CV_8UC3, Scalar (255,255,255));
    for (int i = 0; i < im.cols; i++)
        line(proy, Point(i,im.rows),
             Point (i, im.rows - proj.at(i)),
             Scalar (0,0,0));
    line (proy, Point(0, im.rows - prom),
          Point (im.cols, im.rows - prom), Scalar (0,0,255));
    QImage imagen = cvMatToQImage(proy);
    imagen.save("vertProj.png");
    return proj;
}

QList<Rect> OCV::segmentacionProjectionBased(Mat im, Orientacion_Proyeccion orient)
{
    /*
     * se segmentara la imagen basandose en la proyeccion vertical u orizontal
     * dependiendo del parametro orient. La funcion buscara encontrar los peaks o sea
     * las areas donde las filas o columnas hayan tenido mas pixeles blancos, es en estas
     * zonas donde se encuentran los cuadros de respuestas.
     * Se considera a grandes rasgos un peak una zona en que esta es mayor que el valor
     * base, siendo el valor base el promedio / 2
     * */
    QList <Rect> segmentacion;
    int peakInf = -1, peakSup = -1;
    int prom = 0;
    int base;
    int max = 0;
    bool bEvaluando = false;
    QVector <int> proy;

    //Mat proyec (im.rows, im.cols, CV_8UC3, Scalar (255,255,255));

    if (orient == HORIZONTAL)
        proy = pixHorizontalProj(im);
    else
        proy = pixVerticalProj(im);

    //se calcula el promedio para obtener el valor base
    for (int i = 0; i < proy.size(); i++)
        prom += proy.at(i);

    prom = prom / proy.size();

    base = prom / 2;

    //se recorre el vector de proyeccion buscando los peaks
    for (int i = 0; i < proy.size(); i++)
    {
        /*
         * si se supera la linea base, marcamos el limite inferior y levantamos
         * el flag bEvaluando indicando que estamos buscando el limite superior.
         * La zona entre limite superior e inferior es una region de interes donde
         * se encuentran los cuadros de respuestas
         * */
        if (proy.at(i) > base and !bEvaluando)
        {
            bEvaluando = true;
            peakInf = i;
            max = proy.at(i);
        }
        else if(bEvaluando and proy.at(i) > max)
        {
            // se marca el valor maximo del peak
            max = proy.at(i);
        }
        else if ((bEvaluando and proy.at(i) * 3 < max)
                 or (bEvaluando and proy.at(i) < base)
                 or (i == proy.size() - 1))
        {
            /*
             * cond: si baja de la linea base, el valor es la tercera parte
             * del valor maximo o se llega al fin de la imagen, entonces se
             * considera el fin del peak
             * */
            peakSup = i;
            bEvaluando = false;
            if (peakSup - peakInf < 20)
                continue;


            Rect roi;
            // se asigna una zona de 3 pixeles a cada lado del cuadro
            // como buena practica para q no se encuentre tan pegado
            if (orient == HORIZONTAL)
                roi = Rect(0, peakInf - 3, im.cols,
                          (peakSup - peakInf) + 6);
            else
                roi = Rect(peakInf - 3, 0,
                          (peakSup - peakInf) + 6, im.rows);

            segmentacion.append(roi);

        }
    }

    // filtra las areas que sean de tamaño muy distinta a las demas
    filtradoInterquartil(segmentacion, orient);

    return segmentacion;
}

QList<FIndex> OCV::valorAtipico(QList<FIndex> indices)
{
    /*
     * se aplica el metodo para filtrar valores atipicos propuesto en
     * https://es.wikipedia.org/wiki/Valor_atípico
     * */
    QList <FIndex> filtrados;
    if (indices.isEmpty())
        return filtrados;
    int Q1,Q3;
    float IQR;
    int k, limInf, limSup;
    k = 4; // se busca filtrar los valores atipicos extremos

    Q1 = indices.at(indices.size() / 4).size; // primer quartil
    Q3 = indices.at(indices.size() *3 / 4).size;// tercer quartil

    IQR = Q3 - Q1;
    IQR *= k;
    if (!IQR)
    {
        /*
         * hay casos en los que el rango intercuartil da 0,
         * en estos casos la minima variacion se consideraria
         * un valor atipico, para evitar que esto suceda se establece
         * el margen de 10% del area del peak como nuevo rango
         * intercuaril con el cual filtrar.
         * */
        int Q2= indices.at(indices.size() /2).size; // mediana
        IQR = Q2 * 0.1;
    }

    limInf = Q1 - IQR;
    limSup = Q3 + IQR;

    foreach (FIndex f, indices)
    {
        if (f.size < limInf or f.size > limSup)
            filtrados.append(f);
    }
    return filtrados;
}

void OCV::filtradoInterquartil(QList<Rect> &list,
                               Orientacion_Proyeccion orient)
{
    QList <FIndex> indices;
    //se crea una lista adicional ordenada por tamaño
    for (int i = 0; i < list.size(); i++)
    {
        Rect r = list.at(i);
        FIndex resp;
        resp.indice = i;
        /*
         * se guarda el indice de la lista original de forma
         * de saber cual es la zona que se debe filtrar
         * */
        if (orient == HORIZONTAL)
            resp.size = r.height;
        else
            resp.size = r.width;

        if (indices.isEmpty())
            indices.append(resp);
        else
        {
            //se inserta la nueva respuesta de forma que
            // la lista vaya quedando ordenada por el tamaño
            int pos = 0;
            while (pos < indices.size() and
                   indices.at(pos).size < resp.size)
            {
                pos++;
            }
            indices.insert(pos,resp);
        }
    }

    QList<FIndex> filtrado = valorAtipico(indices);

    foreach (FIndex i, filtrado)
    {
        list.removeAt(i.indice);
    }
    indices.clear();
    filtrado.clear();
}

QList<QList<QRect> > OCV::segmentacionColumnas(Mat im, QList<Rect> &cols, QList<Rect> &rows)
{
    /*
     * Primero se filtran las columnas de acuerdo a cuanto
     * espacio ocupa el contenido. Se busca filtrar la
     * columna de los numeros asumiendo que estos son
     * mas pequeños que los cuadros para marcar respuestas.
     * El criterio de filtrado es el indidce entre el espacio
     * del contenido y el espacio del cuadro, si este indice no
     * es cercano a 1, se incrementa el indice de error. Si el
     * indice de error es muy alto, se filtra esa columna
     * */
    float indiceError;
    for (uint i = 0; i < cols.size(); i++)
    {
        QRect col = rect2QRect(cols.at(i));
        indiceError = 0;
        for (uint j = 0; j < rows.size(); j++)
        {
            QRect row = rect2QRect(rows.at(j));
            //se crea un rectangulo entre la interseccion
            //de la fila con la columna
            QRect inter = col.intersected(row);
            //se fija como region de interes este rectangulo
            Mat roi = im(QRect2Rect(inter));
            vector<vector<Point> > contornos;
            findContours(roi,contornos,CV_RETR_EXTERNAL,
                         CV_CHAIN_APPROX_SIMPLE);
            //se extrae el contorno de la region de interes
            Rect bounding;
            if (contornos.size() > 0)
            {
                //rectangulo minimo que encapsula al contorno
                bounding = boundingRect(contornos[0]);
                //si el rectangulo minimo tiene menos del
                // 80% del tamaño de la region de interes
                //se incrementa el indice de error
                if ((float)bounding.area() /
                        (inter.width() * inter.height()) < 0.5)
                {
                    indiceError++;
                }
            }
            else
                //si no se encontraron contornos se incrementa
                //el indice de error
                indiceError++;
        }
        // se promedia el indice de error
        indiceError /= rows.size();
        /*
         *si al menos la mitad de los bounding rects fueron
         * demasiado pequeños, se elimina la columna
         * */
        if (indiceError > 0.5)
            cols.removeAt(i);
    }

    /*
     *Una vez filtrada la columna del numero se procede a
     * segmentar en columnas basandose en la distancia que
     * existen entre estas. En las columnas que existe mayor
     * distancia es donde se debe segmentar. Se utiliza el
     * enfoque del valor atípico para determinar las columnas
     * a segmentar
     * */
    QList<QList<QRect> > segmentadas;
    //si son solo 2 o menos columnas no hay mucho que segmentar
    if (cols.size() < 3)
    {
        QList<QRect> list;
        foreach (Rect r, cols)
            list.append(rect2QRect(r));
        return segmentadas;
    }


    //insercion ordenada
    QList <FIndex> distancias;
    for (int i = 0; i < cols.size() - 1; i++)
    {
        Rect actual = cols.at(i);
        Rect siguiente = cols.at(i+1);
        FIndex d;
        d.indice = i;
        d.size = siguiente.x - (actual.x + actual.width);
        if (distancias.isEmpty())
            distancias.append(d);
        else
        {
            int pos = 0;
            while (pos < distancias.size() and
                   distancias.at(pos).size < d.size)
            {
                pos++;
            }
            distancias.insert(pos,d);
        }

    }
    QList <FIndex> bordes = valorAtipico(distancias);
    // Se ordenan de acuerdo a los indices, usando burbuja
    for (int i = bordes.size(); i > 0; i--)
        for (int j= 0; j < i - 1; j++)
        {
            if (bordes.at(j).size > bordes.at(j+1).size)
                bordes.swap(j, j+1);
        }

    QList<QRect> temp;
    //se segmentan las columnas
    for (int i = 0; i < cols.size(); i++)
    {
        //se itera hasta llegar al borde
        if (!bordes.isEmpty() and bordes.first().indice == i)
        {
            //una vez en un borde se deja de tomar en cuenta
            //y se agregan las columnas como un bloque a
            //segmentadas
            bordes.removeFirst();
            temp.append(rect2QRect(cols.at(i)));
            segmentadas.append(temp);
            temp.clear();
        }
        else
        {
            /*
             * si no se esta en un borde o la lista esta vacia
             * (ultima iteracion) se agregan las columnas a la
             * lista temporal para luego ser agregadas a la
             * segmentacion
             * */
            temp.append(rect2QRect(cols.at(i)));
        }
    }
    //en la ultima iteracion no se agrega la lista temporal
    segmentadas.append(temp);
    return segmentadas;
}

QList<QList<QRect> > OCV::segmentarRespuestas(QList<QList<QRect> > &cols, QList<Rect> &rows)
{
    /*
     * Para buscar las regiones de las respuestas se calcula la interseccion
     * entre los rectangulos que estan en cols y rows, los primeros al encontrarse
     * separados en las columnas nos permite ordenar facilmente el orden de las
     * respuestas
     * */
    QList <QList<QRect> > respuestas;
    QList <QRect> temp;
    for (int i = 0; i < cols.size(); i++)
    {
        int size = cols.at(i).size();
        foreach (Rect r, rows)
        {
            QRect resp = rect2QRect(r);
            for (int j = 0; j < size; j++)
            {
                QRect inter = resp.intersected(cols.at(i).at(j));
                temp.append(inter);
            }
            respuestas.append(temp);
            temp.clear();
        }
    }

    if (!respuestas.isEmpty())
        cantidadRespuestas = respuestas.at(0).size();

    return respuestas;
}

QList<QList<QRect> > OCV::segmentarRespuestas(QList<Rect> &cols, QList<Rect> &rows)
{
    QList <QRect> temp;
    QList <QList<QRect> > respuestas;

    foreach (Rect r, rows)
    {
        QRect row = rect2QRect(r);

        foreach (Rect c, cols)
        {
            QRect col = rect2QRect(c);
            QRect inter = row.intersected(col);
            temp.append(inter);
        }
        respuestas.append(temp);
        temp.clear();
    }
    return respuestas;
}

QList<int> OCV::identificarRespuestas(Mat im, QList<QList<QRect> > &respSegmentadas)
{
    QList<int> respuestas;
    vector <vector <Point> > contornos;
    bool bBlanco;
    for (int i = 0; i < respSegmentadas.size(); i++)
    {
        int resp = - 1; // -1 para indicar respuesta en blanco
        bBlanco = true;
        int size = respSegmentadas.at(i).size();
        if (i == 59)
        {
            bool b;
            b = false;
        }
        for (int j = 0; j < size; j++)
        {
            Rect roiRect = QRect2Rect(respSegmentadas.at(i).at(j));
            Mat roi = im (roiRect);
            Mat roiCopy = roi.clone();
            findContours(roiCopy, contornos, CV_RETR_EXTERNAL,
                         CV_CHAIN_APPROX_SIMPLE);
            roiCopy.release();
            int areaContorno = 0;
            if (contornos.size() > 1)
            {
                for (int k = 0; k < contornos.size(); k++)
                {
                    int temp = contourArea(contornos[k]);
                    if (temp > areaContorno)
                        areaContorno = temp;
                }
            }
            else
                areaContorno = contourArea(contornos[0]);

            int cantPix = contarPixeles(roi, 250);
            cvtColor(roi, roiCopy, CV_GRAY2BGR);
            drawContours(roiCopy,contornos, 0, Scalar (0,0,255));
            //imwrite("./testc.png", roiCopy);
            float f = (float)cantPix / areaContorno;

            if (f > 0.9)
            {
                // si no estaba en blanco, o sea si se respondio mas de 1 alternativa
                if (!bBlanco)
                {
                    resp = - 2; // -2 para indicar que es respuesta invalida
                    break;
                }
                bBlanco = false;
                resp = j;
            }
        }
        respuestas.append(resp);
    }
    return respuestas;
}

int OCV::contarPixeles(Mat im, int umbral)
{
    /*
     * cuenta la cantidad de pixeles que son mayores
     * que el umbral
     * */
    uchar *ptr;
    int cont  = 0;

    //imwrite("./test.png",im);
    for (int i = 0; i < im.rows; i++)
    {
        ptr = im.ptr(i);

        for (int j= 0; j < im.cols; j++)
        {
            if (ptr[j] > umbral)
                cont++;
        }
    }
    return cont;
}

void OCV::print(Mat im, QList<Rect> &rois, Scalar color,int coord, bool bSaveInDisk)
{
    /*
     * funcion creada para imprimir facilmente las zonas de interes en las imagenes
     * */
    int i = 0;
    foreach (Rect r, rois)
    {
        i++;
        String name;
        stringstream ss;
        if (coord != -1)
            ss << coord << "," <<i;
        else
            ss << i;
        name = ss.str();
        rectangle(im,r, color);
        setLabel(im,name,r);
    }
    if (bSaveInDisk)
    {
        //imwrite("./segmentation.png",im);
    }
}

void OCV::print(Mat im, QList<QRect> &rois, Scalar color, int coord, bool bSaveInDisk)
{
    QList <Rect> lista;
    foreach(QRect r, rois)
    {
        Rect rect = QRect2Rect(r);
        lista.append(rect);
    }
    print (im, lista, color,coord, bSaveInDisk);
    lista.clear();
}


QRect OCV::rect2QRect(Rect r)
{
    return QRect (r.x, r.y, r.width,r.height);
}

Rect OCV::QRect2Rect(QRect r)
{
    return Rect (r.x(), r.y(), r.width(), r.height());
}

