#include "dialogorut.h"
#include "ui_dialogorut.h"

DialogoRut::DialogoRut(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogoRut)
{
    ui->setupUi(this);
}

int DialogoRut::exec(QStandardItemModel *model, QStandardItemModel *model2)
{
    ui->tableView->setModel(model);
    ui->tableView_2->setModel(model2);
    return QDialog::exec();
}

DialogoRut::~DialogoRut()
{
    delete ui;
}
