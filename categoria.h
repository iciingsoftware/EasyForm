#ifndef CATEGORIA_H
#define CATEGORIA_H
#include <QString>


 /// La clase categoria es la clase contenedora de las distintas categorias de roi.
 /** Dentro de EasyForm se distinguen 4 categorías que estan especificadas en el
 * enum TipoCategoria. Esta clasificación sirve para distinguir las regiones
 * de interés que el usuario considera que son útiles para que el software
 * encuentre las alternativas marcadas.
 */

class Categoria
{
public:
    /**
     * @brief Almacena los tipos de categoria que el sistema soporta.
     */
    enum TipoCategoria
    {
        invalida = -1, /*!< indica que esta categoria debería descartarse y no utilizarse. Una categoria es inválida al llamar al constructor vacio */
        aceptado = 0,/*!< Indica que se considera a la región de interes apta para el analisis OMR. */
        rechazado,/*!< Indica que la región de interés se debe ignorar para cualquier análisis. */
        id,/*!< Indica que se debe realizar un análisis OMR en esta roi con el fin de identificar a un alumno. */
        custom/*!< No se utiliza nunca. */
    };
    /// @brief Constructor vacío.
     /** Se inicializa el nombre vacío y ademas el tipo de categoría inválida.
     * Una categoría inicializada como invalida jamás debe utilizarse.
     */
    Categoria ();
    /**
     * @brief Construye una categoría dependiendo de los paramentros nombre y tipo.
     * @param Nombre el nombre que se le asigna a la categoria para mostrarla por pantalla.
     * @param Tipo es usado para discriminar internamente en el programa como debe tratarse la ROI que contenga a esta categoría.
     */
    Categoria(QString nombre, TipoCategoria tipo);
    /**
     *  @brief Destructor por defecto. No hace nada.
     */
    ~Categoria();

    /**
     * @brief Retorna el nombre de la Categoria
     * @return El nombre como QString
     */
    QString getNombre() const;
    /**
     * @brief Sobreescribe el nombre de la categoría.
     * @param Nombre el nuevo nombre de la categoría
     */
    void editarNombre(const QString nombre);
    /**
     * @brief Determina si la categoría tiene un nombre asignado y no es del tipo invalida.
     * @return Verdadero si es válida, falso de lo contrario.
     */
    bool esValida();

    /**
     * @brief Retorna el tipo de la categoría.
     */
    TipoCategoria getTipo() const;

private:
    /**
     * @brief El nombre de la categoria para ser mostrado en pantalla.
     */
    QString nombre;
    /**
     * @brief Determina como debe ser tratada la ROI internamente.
     */
    TipoCategoria tipo;
};

#endif // CATEGORIA_H
