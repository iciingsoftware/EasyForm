#ifndef CSVPARSER_H
#define CSVPARSER_H
#include <QStandardItemModel>


class csvParser
{
public:
    csvParser();
    ~csvParser();

    static QStandardItemModel* readCSV (const QString &path);
    static void writeCSV (QStandardItemModel *model, const QString &path,
                          QIODevice::OpenMode mode = QIODevice::ReadOnly);
};

#endif // CSVPARSER_H
