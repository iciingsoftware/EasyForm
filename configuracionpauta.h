#ifndef CONFIGURACIONPAUTA_H
#define CONFIGURACIONPAUTA_H

#include <QPolygon>
#include <QStandardItemModel>
#include <QList>
#include "categoria.h"

/// Es la clase contenedora que guarda la información necesaria para la sesión.
/**
 * Esta clase es usada por el programa para saber que hacer con un formulario, las
 * demas clases tendrán en cuenta lo que esta almacenado acá para ajustar su
 * propio comportamiento.
 * Por ejemplo la clase Estadistica se comportará completamente distinta dependiendo
 * si el usuario desea analizar un formulario evaluable, frente a uno que no lo es.
 */
class ConfiguracionPauta
{
public:
    /// Estructura que encapsula las regiones de interés.
    /**
     * En esta estructura se almacenan los componentes que se consideran importantes
     * para trabajar con las ROI. Esta structura es usada en varias partes del programa
     * a la hora de obtener las zonas importantes de la imagen, acotarla y hacerla mas
     * manejable.
     */
    struct roiStruct
    {
        QString nombreRoi /*!< Nombre de la ROI, usado para distinguirlo en la interfaz gráfica, no pueden haber dos ROI con el mismo nombre*/;
        QPolygon roi;/*!< Almacena los puntos x,y de la región de interés y es correlativo a los pixeles de la imagen*/
        Categoria categoria;/*!< la categoria a la cual el usuario asignó esta ROI*/
        int Posicion; /*!< La posición que ocupa dentro de la categoria, ésta es importante puesto que se utiliza la posicion para calcular el número de la respuesta.*/
    };
    /// Constructor principal.
    /**
     * En este constructor se define antes de crear la clase las categorías que se desean.
     * No se crean ROI con este constructor.
     * @param listaCategorias Una QList con las categorías ya creadas.
     */
    ConfiguracionPauta (QList <Categoria> listaCategorias);
    /// Constructor sobrecargado
    /**
     * Construye una configuracion vacía, sin Categorias ni ROI.
     * No se puede utilizar esta configuracion a menos que se le asignen categorias
     * y ROIs.
     */
    ConfiguracionPauta();
    /// Destructor.
    /// Libera la memoria al vaciar las listas.
    ~ConfiguracionPauta();

    /// Busca las ROIs que se asignaron TipoCategoria::aceptado
    /**
     * Recorre la lista de ROIs y retorna una lista con rectangulos que envuelve
     * a aquellas que el usuario determino que son las que contienen las
     * respuestas que se desean buscar
     *
     * @return Una lista con los boundingRect() de las ROIs en la categoria aceptado.
     */
    QList <QRect> obtenerAceptados ();
    QRect obternerRoiIdentificacion();
    /// Se cambia el nombre de la categoria antigua a nueva
    void editarCategorias (QString antigua, QString nueva);
    /// Se agrega una nueva región de interés al final de la lista rois
    void agregarROI (roiStruct roi);
    /// Retorna un puntero a la lista rois
    inline  QList <roiStruct> *getROIs(){return rois;}
    /// Busca la ROI con el nombre del parámetro.
    /**
     * Recorre la lista rois comparando los nombres, si lo encuentra
     * retorna el indice donde se encuentra la estructura completa.
     * @param nombre Nombre de la ROI que se desea
     * @return Indice donde se encuentra en la lista rois
     */
    int getIndexROI (QString nombre);
    /// Retorna la categoria de nombre... nombre.
    Categoria getCategoria (QString nombre);
    /// Retorna la categoria de TipoCategoria tipo
    Categoria getCategoria(Categoria::TipoCategoria tipo);
    /// Set evaluable
    inline void setEvaluable (bool b) {evaluable = b;}
    /// Get evaluable
    inline bool esEvaluable (){return evaluable;}
    /// Set respuestas correctas.
    inline void setRespuestasCorrectas (QList <int> resp) {respCorrectas = resp;}
    /// Get respuestas correctas.
    inline QList <int> getRespuestasCorrectas () {return respCorrectas;}
    /// inicializa las categorias por defecto.
    /**
     * Crea las categorias por defecto. Estas son:
     *
     * - Aceptados
     * - Rechazados
     * - Identificación.
     *
     * Además retorna una lista de string con los nombres de estas categorias
     * para que la interfaz se encargue de mostrarlos por pantalla.
     * @return una lista de string para que sean mostrados por pantalla.
     */
    QList <QString> inicializarCategorias ();

private:
    /**
     * @brief Determina si el formulario se compara con otro con respuestas correctas.
     *
     * Se considera para un formulario evaluable que se tiene presente una plantilla con las respuestas correctas,
     * las cuales serán comparadas con las respuestas de los otros formularios y se obtendra una evaluación
     * Para un formulario no evaluable esto no es necesario, pero se obtienen otros tipos de estadísticas.
     */
    bool evaluable;
    /**
     * @brief Lista con las regiones de interés que encontró el programa y las que el usuario definió.
     */
    QList <roiStruct> *rois;
    /**
     * @brief Listas con las categorias que soporta la sesión.
     */
    QList <Categoria> listaCategorias;
    /**
     * @brief Lista con las respuestas correctas que definió el usuario para el caso de un formulario evaluable.
     */
    QList <int> respCorrectas;

};



#endif // CONFIGURACIONPAUTA_H
