#include "treeclasificacionwrappercontroller.h"
#include <QDebug>

ConfiguracionInterface::ConfiguracionInterface(ConfiguracionPauta *pConf)
{
    this->pConf = pConf;
}

ConfiguracionInterface::~ConfiguracionInterface()
{

}


bool ConfiguracionInterface::estaContenido(const QString &valor)
{
    /*comprueba que no se desee ingresar un nombre que ya este contenido en
     * otra region de interes.
     * La funcion itera sobre la lista de regiones de interes y consulta
     * si el nombre es igual al argumento, de ser verdadero, retorna true
     * o sea que el nombre ya se encuentra contenido. Si al llegar al fin
     * de la lista no se encuentra, se retorna false
     */
    QList <ConfiguracionPauta::roiStruct>* listaROI = pConf->getROIs();
    for (int i = 0; i < listaROI->size(); i++)
    {
        if (listaROI->at(i).nombreRoi.compare(valor) == 0)
            return true;
    }
    return false;
}

void ConfiguracionInterface::actualizarNombre(const QString &antiguo, const QString &nuevo)
{
    /* actualiza el nombre de la region de interes "antigua" a "nueva"
     * se asume que ya se comprobo que el nombre no esta repetido y que
     * "antigua" efectivamente corresponde al nombre de algun ROI contenido
     */
    int indice = pConf->getIndexROI(antiguo);
    if (indice >= 0 and indice < pConf->getROIs()->size())
        (*pConf->getROIs())[indice].nombreRoi = nuevo;
    else
        pConf->editarCategorias(antiguo, nuevo);
}

bool ConfiguracionInterface::estaAceptado(const QString nombre)
{
    int indice = pConf->getIndexROI(nombre);
    if (pConf->getROIs()->at(indice).categoria.getTipo() == Categoria::aceptado)
        return true;
    return false;
}



void ConfiguracionInterface::actualizarCategoria(const QString &elem, const QString &cat, QModelIndex index)
{
    /*Mueve una region de interes de una categoria a otra
     */
    //se obtiene la categoria a la que se desea asignar el ROI
    Categoria nuevaCat = pConf->getCategoria(cat);
    //se obtiene el indice en la lista de la region de interes
    int indice = pConf->getIndexROI(elem);
    //se asigna al ROI la nueva categoria
    QList <ConfiguracionPauta::roiStruct> *roiList = pConf->getROIs();

    if (!index.isValid())
    {
        int maxIndex = -1;
        for (int i = 0; i < roiList->size(); i++)
        {
            if (roiList->at(i).categoria.getTipo() == Categoria::aceptado)
            {
                int posRoi = roiList->at(i).Posicion;
                if (posRoi > maxIndex)
                    maxIndex = posRoi;
            }
        }
        (*roiList)[indice].Posicion = maxIndex + 1;

    }
    else
    {
        for (int i = 0; i < roiList->size(); i++)
        {
            if (roiList->at(i).categoria.getTipo() == Categoria::aceptado)
            {
                if (roiList->at(i).Posicion > index.row())
                    (*roiList)[i].Posicion = roiList->at(i).Posicion + 1;
            }
        }
        (*roiList)[indice].Posicion = index.row()+1;
    }

    (*roiList)[indice].categoria = nuevaCat;
}

void ConfiguracionInterface::appendROI(QString nombre, QPolygon poly)
{
    Categoria cat = pConf->getCategoria(Categoria::rechazado);
    ConfiguracionPauta::roiStruct roi;
    roi.categoria = cat;
    roi.nombreRoi = nombre;
    roi.roi = poly;
    pConf->getROIs()->append(roi);
}

QList<QString> ConfiguracionInterface::inicializarCategorias()
{
    return  pConf->inicializarCategorias();
}

ConfiguracionPauta::roiStruct ConfiguracionInterface::getRegionOfInterest(QString nombre)
{
    int indice = pConf->getIndexROI(nombre);
    return pConf->getROIs()->at(indice);
}





