#include "treeclasificacionmodel.h"

TreeClasificacionModel::TreeClasificacionModel(QObject *parent)
    :QStandardItemModel(parent)
{
    selection = 0;
    treeController = 0;
}

TreeClasificacionModel::~TreeClasificacionModel()
{

}

bool TreeClasificacionModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    if (!selection)
        return false;

    QModelIndex actual = selection->currentIndex();
    if (!actual.isValid())
        return false;

    QModelIndex pParent = parent.parent();
    QModelIndex actParent = actual.parent();
    if (row == -1)
    {
        if (!actParent.isValid() or !parent.isValid())
            return false; // se intenta mover una categoria

        if (!pParent.isValid())
        /*
         * Se dropea en un item del nivel mas alto, por lo que
         * se puede dejar que qt lo administre
         * */
        {
            if (treeController)
            {
                treeController->actualizarCategoria(itemFromIndex(actual)->
                                         data(Qt::DisplayRole).toString(),
                                         itemFromIndex(parent)->
                                         data(Qt::DisplayRole).toString(),
                                         QModelIndex());

                ConfiguracionPauta::roiStruct roi = treeController->getRegionOfInterest(actual.
                                                        data(Qt::DisplayRole).toString());
                emit (cambioCategoria(roi));
            }
            return QStandardItemModel::dropMimeData(data, action,
                                                 row, column,
                                                 parent);
        }
        else
        {
        /*
         * si se dropea un item al segundo nivel nosotros nos
         * encargamos de manejarlo,
         * */
            QStandardItem  *item = itemFromIndex(actual);
            QVariant v = item->data(Qt::DisplayRole);
            QString name = v.toString();
            //b =removeRow(actual.row(),actParent);
            insertRow (parent.row()+1,pParent);
            QModelIndex destino = index(parent.row() +1, 0,
                                        pParent);

            item = itemFromIndex(destino);
            item->setData(name, Qt::DisplayRole);
            if (destino.parent() == actual.parent() and
                    destino.row() < actual.row())
                removeRow(actual.row() +1 ,actParent);
            else
            {
                if (treeController)
                {
                    treeController->actualizarCategoria(itemFromIndex(actual)->
                                             data(Qt::DisplayRole).toString(),
                                             itemFromIndex(destino.parent())->
                                             data(Qt::DisplayRole).toString(),
                                             destino);
                    //obtiene la categoria a la cual se envío la ROI
                    ConfiguracionPauta::roiStruct roi = treeController->getRegionOfInterest(actual.
                                                            data(Qt::DisplayRole).toString());
                    emit (cambioCategoria(roi));
                }
                removeRow(actual.row(), actParent);
            }
        }

        return false;
    }
    else
    {
        // se intenta dropear un item del nivel superior a uno
        //de un nivel inferior, esta operacion no esta permitida
        /*
        if (!actParent.isValid())
            return false;

        if (treeController)
        {
            treeController->actualizarCategoria(itemFromIndex(actual)->
                                     data(Qt::DisplayRole).toString(),
                                     itemFromIndex(parent)->
                                     data(Qt::DisplayRole).toString());
            emit (cambioCategoria(treeController));
        }
        return QStandardItemModel::dropMimeData(data, action,
                                             row, column,
                                             parent);
    */
        return false;
    }
}

void TreeClasificacionModel::setSelection(QItemSelectionModel *value)
{
    selection = value;
}

void TreeClasificacionModel::setTreeController(ConfiguracionInterface *value)
{
    treeController = value;
}

bool TreeClasificacionModel::setData(const QModelIndex &index, const QVariant &value, int role)
{

    QList<QStandardItem *> r = findItems(value.toString(),
                                         Qt::MatchFixedString|
                                         Qt::MatchRecursive);


    bool b1 = Qt::EditRole == role;
    bool b2 = index.isValid();
    bool b3 = !r.isEmpty();
    if (b1 and b2 and b3)
    {
        return false;
    }
    else
    {
        QStandardItem *i = itemFromIndex(index);
        QString antiguo = i->data(Qt::DisplayRole).toString();
        treeController->actualizarNombre(antiguo,value.toString());
        emit(cambioNombre(antiguo, value.toString()));
        return QStandardItemModel::setData(index,value,role);
    }
}

QList<QRect> TreeClasificacionModel::getAceptados(QList<QPair<QPolygon, QString> > polygonos)
{
    /*QList <QRect> aceptados;
    QString nombreCat;

    nombreCat = treeController->getCatAceptado();

    QList <QStandardItem*> items;
    items = findItems(nombreCat);

    for (int i = 0; i < items.first()->rowCount(); i++)
    {
        QStandardItem *item = items.first()->child(i);
        QString nombre;
        nombre = item->data(Qt::DisplayRole).toString();
        QPolygon pol;
        for (int j = 0; j < polygonos.size(); j++)
        {
            if (QString::compare(nombre, polygonos.at(j).second) == 0)
            {
                pol = polygonos.at(j).first;
                break;
            }
        }
        aceptados.append(pol.boundingRect());
    }
    return aceptados;*/
}

