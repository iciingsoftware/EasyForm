#ifndef DIALOGORUT_H
#define DIALOGORUT_H

#include <QDialog>
#include <QStandardItemModel>

namespace Ui {
class DialogoRut;
}

class DialogoRut : public QDialog
{
    Q_OBJECT

public:
    explicit DialogoRut(QWidget *parent = 0);
    int exec(QStandardItemModel* model, QStandardItemModel* model2);
    ~DialogoRut();

private:
    Ui::DialogoRut *ui;
};

#endif // DIALOGORUT_H
