SOURCES += \
    main.cpp \
    mainwindow.cpp \
    ocv.cpp \
    imagevisualizer.cpp \
    configuracionpauta.cpp \
    treeclasificacionmodel.cpp \
    treeclasificacionproxystyle.cpp \
    treeclasificacionwrappercontroller.cpp \
    categoria.cpp \
    csvparser.cpp \
    dialogorut.cpp

QT += widgets

INCLUDEPATH += C:\\opencv\\build\\x86\\Mingw\\install\\include

LIBS += -LC:\\opencv\\build\x86\\Mingw\\install\x64\\mingw\\lib \
        -lopencv_core2410.dll \
        -lopencv_highgui2410.dll \
         -lopencv_imgproc2410.dll \
        -lopencv_features2d2410.dll \
        -lopencv_calib3d2410.dll


FORMS += \
    mainwindow.ui \
    dialogorut.ui

HEADERS += \
    mainwindow.h \
    ocv.h \
    imagevisualizer.h \
    configuracionpauta.h \
    treeclasificacionmodel.h \
    treeclasificacionproxystyle.h \
    treeclasificacionwrappercontroller.h \
    categoria.h \
    csvparser.h \
    dialogorut.h

RESOURCES += \
    iconos.qrc
