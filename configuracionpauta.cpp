#include "configuracionpauta.h"

ConfiguracionPauta::ConfiguracionPauta(QList <Categoria> listaCategorias)
{
    this->listaCategorias = listaCategorias;
    rois = new QList <roiStruct>;
}

ConfiguracionPauta::ConfiguracionPauta()
{
    /*Constructo vacio
     */
    rois = new QList <roiStruct>;
    /*
    Categoria cat1 ("Aceptados", Categoria::aceptado);
    Categoria cat2 ("Rechazados", Categoria::rechazado);
    listaCategorias << cat1 << cat2;
    */
}

ConfiguracionPauta::~ConfiguracionPauta()
{
    /*Destructor
     */
    listaCategorias.clear();;
    rois->clear();
    respCorrectas.clear();
    delete rois;
}

QList<QRect> ConfiguracionPauta::obtenerAceptados()
{
    /* Retorna un rectangulo que envuelve la zona de interes que el
     * usuario ha seleccionado
     */
    QList <QRect> listaAceptados;

    for (int i = 0; i < rois->size() ; i++)
    {
        if (rois->at(i).categoria.getTipo() == Categoria::aceptado)
            listaAceptados.append(rois->at(i).roi.boundingRect());
    }
    return listaAceptados;
}

QRect ConfiguracionPauta::obternerRoiIdentificacion()
{
    for (int i = 0; i < rois->size(); i++)
    {
        if (rois->at(i).categoria.getTipo() == Categoria::id)
            return rois->at(i).roi.boundingRect();
    }
    return QRect();
}

void ConfiguracionPauta::editarCategorias(QString antigua, QString nueva)
{
    for (int i= 0; i < listaCategorias.size(); i++)
    {
        if (listaCategorias.at(i).getNombre().compare(antigua) == 0)
            listaCategorias[i].editarNombre(nueva);

    }
}

void ConfiguracionPauta::agregarROI(ConfiguracionPauta::roiStruct roi)
{
    /*Agrega una nueva region de interes al final de la lista
     */
    rois->append(roi);
}

int ConfiguracionPauta::getIndexROI(QString nombre)
{
    /*obtiene el indice de una region de interes basado en su nombre
     */
    for (int i = 0; i < rois->size(); i++)
    {
        if (rois->at(i).nombreRoi.compare(nombre) == 0)
            return i;
    }
    return -1;
}

Categoria ConfiguracionPauta::getCategoria(QString nombre)
{
    Categoria c;
    foreach (c, listaCategorias)
    {
        int value = c.getNombre().compare(nombre);
        if (value == 0)
            return c;
    }
    return Categoria();
}

Categoria ConfiguracionPauta::getCategoria(Categoria::TipoCategoria tipo)
{
    foreach (Categoria c, listaCategorias)
    {
        if (c.getTipo() == tipo)
            return c;
    }
    return Categoria();
}

QList<QString> ConfiguracionPauta::inicializarCategorias()
{
    /* Esta funcion se encarga de crear las categorias ("aceptado", "rechazado", identificacion"),
     * etc. Retorna ademas una lista con los nombres de las categorias que se asignaron
     * automaticamente para que luego estas se muestren por pantalla a travez de la
     * interfaz.
     * el orden es:
     * 1. Aceptados
     * 2. Rechazados
     * 3. Identificacion
     */
    QString nombreCat1("Aceptados");
    QString nombreCat2 ("Rechazados");
    QString nombreCat3 ("Identificación");
    Categoria cat1 (nombreCat1, Categoria::aceptado);
    Categoria cat2 (nombreCat2, Categoria::rechazado);
    Categoria cat3 (nombreCat3, Categoria::id);
    listaCategorias << cat1 << cat2 << cat3;
    QList <QString> lista;
    lista << nombreCat1 << nombreCat2 << nombreCat3;
    return lista;
}

