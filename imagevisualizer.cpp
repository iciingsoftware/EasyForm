#include "imagevisualizer.h"
#include <QWheelEvent>
#include <math.h>
#include <QDebug>
#include <QPainter>

ImageVisualizer::ImageVisualizer(QWidget *parent)
    :QLabel(parent)
{
    pan = false;
    panX = 0;
    panY = 0;
    escalaActual = 1;
    indiceResaltado = -1;
    debug = false;
    conf = 0;
    rubberBand = 0;
}

ImageVisualizer::~ImageVisualizer()
{
}

void ImageVisualizer::setPixmap(const QPixmap &pixmap, ConfiguracionPauta *conf)
{
    this->conf = conf;
    pix = pixmap.copy();
    escalaActual = 1;

    /****************pintamos los bordes de los polygonos***********/
    QPainter painter (&pix);
    QPen pen (Qt::blue);
    pen.setWidth(3);
    painter.setPen(pen);

    //QPair<QPolygon, QString> p;
    QList <ConfiguracionPauta::roiStruct> *roi = conf->getROIs();
    /*
    foreach(QPolygon p, (*roi))
    {
        painter.drawPolygon(p);

    }
    */
    for (int i = 0; i < roi->size(); i++)
    {
        painter.drawPolygon(roi->at(i).roi);
    }

    // se pinta el interior de los bordes
    pintarCuadrosAceptados();
}

void ImageVisualizer::wheelEvent(QWheelEvent *event)
{
    if (event->modifiers().testFlag(Qt::ControlModifier))
    {
        int numSteps = event->delta() / 15 / 8;
        //formula estandar para determinar los step de la ruedita
        if (numSteps == 0)
        {
            event->ignore();
            return;
        }
        double scaleFactor = pow(1.25 , numSteps);//el factor de escala es de 1,25
        escalarImagen(scaleFactor);
        event->accept();
        return;
    }
    QLabel::wheelEvent(event);// si no se apreto ctrl + wheel se deja a qlabel q maneje el evento
}

void ImageVisualizer::escalarImagen(double factorEscala)
{
    escalaActual *=  factorEscala;
    pintarCuadrosAceptados();
}

void ImageVisualizer::mousePressEvent(QMouseEvent *e)
{
    if (!conf)
        return;
    rubberBandStartPoint = e->pos();
    if (rubberBand == 0)
        rubberBand = new QRubberBand(QRubberBand::Rectangle,this);
    rubberBand->setGeometry(QRect (rubberBandStartPoint, QSize()));
    rubberBand->show();
}

void ImageVisualizer::mouseMoveEvent(QMouseEvent *e)
{
    if (!conf)
        return;
    rubberBand->setGeometry(QRect (rubberBandStartPoint, e->pos()).normalized());
}

void ImageVisualizer::mouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED (e);
    if (!conf)
        return;
    rubberBand->hide();
    crearROI();
}

void ImageVisualizer::crearROI()
{
    QRect roi (rubberBand->geometry().normalized());

    /********verificar que no se salga de los limites**********/
    if (roi.left() < 0)
        roi.setLeft(0);
    if (roi.top() < 0)
        roi.setTop(0);
    if (roi.right() > pix.width() * escalaActual)
        roi.setRight(pix.width() *escalaActual);
    if (roi.bottom() > pix.height() * escalaActual)
        roi.setBottom(pix.height() * escalaActual);
    /*************** se compensa el factor de escala *************/

    roi.setLeft(roi.left() / escalaActual);
    roi.setRight(roi.right() / escalaActual);
    roi.setTop(roi.top() / escalaActual);
    roi.setBottom(roi.bottom()/escalaActual);

    emit (usuarioCreoROI(QPolygon(roi)));
}



void ImageVisualizer::pintarCuadrosAceptados()
{

    if (!conf)
        return;
    //se pintan los cuadros que estan checkeados en el treewidget
    QPixmap actual;
    actual = pix.scaled(pix.size()* escalaActual); //imagen que se le aplico el factor de escala
    QPainter painter (&actual);//nos disponemos a pintar sobre el pixmap
    QBrush brush (QColor(0,200,0, 50));//color verde con patron rayado diagonal
    painter.setBrush(brush);
    QFont font("Times", 24, QFont::Bold);
    QFontMetrics fm (font);
    painter.setFont(font);

    if (debug)
        debug = false;//para depuracion, no tiene mayor relevancia

    QList <ConfiguracionPauta::roiStruct> *roi = conf->getROIs();
    for (int i=0; i< roi->size(); i++)
    {
        //recorre la lista con los poligonos para dibujarlos
        QPolygon poly = roi->at(i).roi;
        for (int j=0; j < poly.size(); j++)
        {
            //recorro los puntos del poligono para aplicar la escala
            poly[j] *= escalaActual;
        }
        //QString texto = conf->regionInteres.at(i).second;
        QString texto = roi->at(i).nombreRoi;
        QRect bounding = fm.boundingRect(poly.boundingRect()
                                         , Qt::AlignCenter,
                                         texto);
        painter.fillRect(bounding, Qt::white);
        painter.drawText(bounding, Qt::AlignCenter, texto);
        if (!treeCategorias->estaAceptado(texto))
            continue;
        //si el poligono no esta checkeado en treewidget no se pinta

        //si es el indice seleccionado en tree widget se pinta azul
        if (i == indiceResaltado)
        {
            QBrush bResaltado (QColor(Qt::blue), Qt::BDiagPattern);
            painter.save();
            painter.setBrush(bResaltado);
            painter.drawPolygon(poly);
            painter.restore();
        }
        else
            painter.drawPolygon(poly);
    }

    QLabel::setPixmap(actual);
}

void ImageVisualizer::actualizar()
{
    //QPolygon roi = conf->regionInteres.last().first;
    QPolygon roi = conf->getROIs()->last().roi;
    QPainter painter (&pix);
    QPen pen (QBrush(QColor (0,0,255)),2);
    painter.setPen(pen);

    painter.drawPolygon(roi);
    pintarCuadrosAceptados();
}




ConfiguracionInterface *ImageVisualizer::getTreeCategorias() const
{
    return treeCategorias;
}

void ImageVisualizer::setTreeCategorias(ConfiguracionInterface *value)
{
    treeCategorias = value;
}

QPixmap ImageVisualizer::getPix() const
{
    return pix;
}

void ImageVisualizer::setPix(const QPixmap &value)
{
    pix = value;
    escalaActual = 1;
    QLabel::setPixmap(pix);
}


void ImageVisualizer::setIndiceResaltado(int indice)
{
    indiceResaltado = indice;
    debug = true;
    pintarCuadrosAceptados();
}

void ImageVisualizer::setaceptados(int indice)
{
    //esta funcion es invocada cada vez que se edita un item en
    //treewidget, no sabemos si esta checkeandolo o descheckeandolo
    //por eso se cambia al estado contrario del actual solamente
    bool actual = aceptados.at(indice);
    if (actual)
        aceptados[indice] = false;
    else
        aceptados[indice] = true;
    pintarCuadrosAceptados();
}



