#ifndef TREECLASIFICACIONMODEL_H
#define TREECLASIFICACIONMODEL_H

#include <QStandardItemModel>
#include <QItemSelectionModel>
#include "treeclasificacionwrappercontroller.h"
#include <QPair>

/**
 * @brief Modelo que para construcción de árboles de solo 2 niveles.
 *
 * La idea de esta clase es que permita realizar operaciones de drag and drop
 * pero solo del segundos niveles hacia el segundo nivel, o sea no se permite
 * arrastrar un item que se encuentra en el segundo nivel hacia el primero ni vise
 * versa. Realizar esta tarea requiere imponer restricciones al QStandardItemModel
 * que si permite estas operaciones.
 */
class TreeClasificacionModel: public QStandardItemModel
{
    Q_OBJECT
public:
    TreeClasificacionModel(QObject *parent = 0);
    ~TreeClasificacionModel();

    /**
     * @brief Función que gestiona donde insertar despues de una operación de drag and drop.
     *
     * En dropMimeData se consultan diferentes casos posibles, dependiendo de cual
     * sea éste se actúa en concordancia, en algunos casos la operación se interrumpe
     * porque no cumple con los requisitos de ser solamente dentro del segundo nivel
     * del árbol. Una vez que se completo una operación se lanza la signal
     * cambioCategoria para que se pueda actualizar el ImageVisualizer con los
     * cambios que realizó el usuario.
     * @param data
     * @param action
     * @param row
     * @param column
     * @param parent
     * @return
     */
    bool dropMimeData(const QMimeData *data,
                      Qt::DropAction action,
                      int row, int column,
                      const QModelIndex &parent);

    void setSelection(QItemSelectionModel *value);
    void setTreeController (ConfiguracionInterface *value);
    /**
     * @brief Cambia el nombre de una categoria o un ROI.
     *
     * Para que la operación sea exitosa se necesita que el nuevo nombre
     * no se este repetido, por lo que primero se comprueba eso, de ser
     * unico el nuevo nombre se permite el cambio.
     *
     * @param index El indice donde se encuentra dentro del modelo.
     * @param value El nuevo valor que se desea que contenga
     * @param role El rol al cual asignar el valor, se desea que sea Qt::DisplayRole
     * @return true si la operación fue exitosa, falso de lo contrario
     */
    bool setData (const QModelIndex &index, const QVariant &value, int role);
    QList <QRect> getAceptados (QList <QPair <QPolygon,QString> > polygonos);

signals:
    /**
     * @brief Signal que se dispara cuando se cambia de categoria a un ROI
     * @param controller
     */
    void cambioCategoria (ConfiguracionPauta::roiStruct roi);
    /**
     * @brief Signal que se dispara cuando se cambia el nombre a un ROI o una categoria
     * @param antiguo
     * @param nuevo
     */
    void cambioNombre (QString antiguo, QString nuevo);

private:
    const QItemSelectionModel *selection;
    ConfiguracionInterface *treeController;
};

#endif // TREECLASIFICACIONMODEL_H
