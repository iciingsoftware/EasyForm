#include "categoria.h"

Categoria::Categoria()
{
    nombre = QString();
    tipo = invalida;
}

Categoria::Categoria(QString nombre, TipoCategoria tipo)
{
    this->nombre = nombre;
    this->tipo = tipo;
}

Categoria::~Categoria()
{

}
QString Categoria::getNombre() const
{
    return nombre;
}

void Categoria::editarNombre(const QString nombre)
{
    this->nombre = nombre;
}

bool Categoria::esValida()
{
    if (nombre.isEmpty() or tipo == -1)
        return false;
    return true;
}

Categoria::TipoCategoria Categoria::getTipo() const
{
    return tipo;
}




