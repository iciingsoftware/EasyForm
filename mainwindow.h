#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>
#include <QFileDialog>
#include <QScrollBar>
#include <vector>
#include <opencv2/core/core.hpp>
#include <QTreeWidgetItem>
#include "configuracionpauta.h"
#include "treeclasificacionmodel.h"
#include "treeclasificacionproxystyle.h"
#include "treeclasificacionwrappercontroller.h"

namespace Ui {
class MainWindow;
}

/**
 * @brief Clase principal que gestiona todo.
 *
 * La clase MainWindows es la que se encarga de mostrar la interfaz gráfica y
 * además gestionar el flujo del programa.
 * Crea la configuración, gestiona la interacción con el usuario y también
 * solicita que se analice la ROI en busca de las respuestas.
 * Todos los cambios en el comportamiento dinamico de los widget se puede
 * hacer desde esta clase: agregar animaciones, tooltips, colores, gestionar
 * el patrón modelo item view. Con respecto a estos cambios basta con leer la
 * documentación de qt apretando F1 sobre alguna clase.
 *
 * Las funciones mas importantes son:
 *
  ~~~~~~~~~~~~~~~~{.cpp}
  loadImage();
  ~~~~~~~~~~~~~~~~
 * que se encarga de cargar una imagen y buscar ROIs automaticamente.
 *
 *
 ~~~~~~~~~~~~~~~~{.cpp}
    realizarOMR();
 ~~~~~~~~~~~~~~~~
 Esta función es magica y es la que encuentra las respuestas en los formularios
  ~~~~~~~~~~~~~~~~{.cpp}
  aceptarConfiguración ();
  ~~~~~~~~~~~~~~~~
 *
 * que se encarga de recorrer la carpeta y realizar el omr según lo que el
 * usuario haya configurado.
 *
 * La función llenar tree widget funciona a grandes rasgos:
 * Creando un modelo, llenarlo con QStandarItems que a su vez tienen otros item
 * como hijos. Los items padres representan las categorias y los hijos, las ROI
 ~~~{.cpp}
  llenarTreeWidget()
  {
    model = new TreeClasificacionModel; // crea un nuevo modelo. Según el patrón item view controller
    QStandardItem *cat = new QStandardItem ("nombre categoria"); // un nuevo elemento
    model->setItem(posicion, cat); // se inserta la categoria en su posicion
    QStandardItem *child = new QStandardItem ("nombre roi"); // se crea un nuevo item para los roi hijos
    model->item(1)->appendRow(child); // se inserta el roi en la categoria rechazado, la posicion 1
    ui->treeView->setModel(model); // finalmente se asigna el modelo a la vista que se encarga de mostrarlo por pantalla
  }
 ~~~
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT // macro para permitir la aplicacion de las signals y slots

public:
    /// constructor por defecto de qt
    explicit MainWindow(QWidget *parent = 0);
    /// destructor
    ~MainWindow();
public slots:
    /** carga una imagen y la procesa.
     *
     * LoadImage no solo carga una imagen, sino que se la entrega a la clase OCV
     * para que la procese en busca de ROIs, las cuales son presentadas al usuario
     * para que decida en cual de ellas se encuentran las respustas.
     * @brief loadImage
     */
    void loadImage ();
    /**
     * @deprecated no hace nada.
     */
    void cambiarSeleccion ();
    /**
     * @deprecated no hace nada.
     */
    void onItemChanged (QTreeWidgetItem *item);
    /**
     * @brief Función que se ejecuta cuando el usuario apreta el botón aceptar.
     *
     * Esta función se encarga de escribir los ultimos cambios a la configuración
     * antes de ejecutar el algoritmo de OMR. Estos cambios son:
     * - Si el formulario es evaluable.
     * - Las respuestas correctas en caso de los formularios evaluables.
     *
     * posteror a esto llama a la funcion que se encarga de gestionar el OMR.
     */
    void aceptarConfiguracion ();
    /// Cambia el nombre de una ROI
    void actualizarNombre (QString viejo, QString nuevo);
    /// Setea la carpeta donde se encuentran las imagenes para analizar
    void setFolder ();
    /// Agrega una roi a treeWidget para mostrarlo por pantalla
    void appendNewROI (QPolygon roi);
    void onCambioCategoria (ConfiguracionPauta::roiStruct roi);
    void ImportarCSV();

signals:
    void seleccionCambio (int);
    void aceptadoCambio (int);

private:
    int contRoiUsuario;
    /**
     * @brief Se encarga de crear los item para mostrar por pantalla las categorias y los roi.
     *
     * Esta función crea las categorias y ademas muestra por pantalla las
     * regiones de interés que se detectaron automáticamente.
     * @param rect
     */
    void llenarTreeWidget (QList<QPolygon> rect);
    /**
     * @brief Es magia.
     *
     * Esta función recorre la carpeta especificada cargando las imagenes que están en
     * ella, posteriormente se las pasa a OCV para que busque las respuestas.
     * Una vez completado el analisis OMR se calculan las funciones estadisticas y se puebla
     * la vista que se encargará de presentarselas al usuario.
     * @param folder
     */
    void realizarOMR (QString folder);
    float calcularPromedioUniverso (QList <float> promedios);
    float calcularDesviacionEstandar (QList <float> promedios, float prom);
    QPair <int, int> respuestaMasAcertada();
    QPair <int, int> respuestaMasErronea ();
    void insertarEstadisticaHoja (QList <float> promedios, QStringList listaRut);
    /// convierte entre el formato de poligonos de opencv al de qt que es mas fácil de manejar.
    QList <QPolygon> transformContour2Poly (std::vector<std::vector<cv::Point> >rect);
    void poblarTablaEvaluables (QList <float> promedios);
    void poblarTablaNoEvaluables(QStandardItemModel *respuestas, int cantAlternativas);
    QString buscarNombreDeRut(QString rut);
    QString eliminarFormatoRut (QString rut);
    /**
     * @brief lista con las respuestas entregadas por el analisis OMR
     */
    QList <QList <int> > respuestasOMR;

    Ui::MainWindow *ui;
    QImage imagen;
    QImage imagen2;
    ConfiguracionPauta *pConf;
    TreeClasificacionModel *model;
    ConfiguracionInterface *treeController;
    QStandardItemModel *rutAlumnos;

};

#endif // MAINWINDOW_H
