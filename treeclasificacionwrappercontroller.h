#ifndef CONFIGURACIONINTERFACE_H
#define CONFIGURACIONINTERFACE_H
#include <QString>
#include <QStringList>
#include <QPair>
#include "configuracionpauta.h"
#include <QModelIndex>

/**
 * @brief Clase Wrapper de ConfiguracionPauta.
 *
 * La idea es que las demas clases no interactúen directamente con
 * ConfiguracionPauta a la hora de modificar cosas ahí, sino que lo hagan
 * mediante esta clase wraper que tiene ademas lógica extra de control.
 * Las funciones son casi idem a las de ConfiguracionPauta asi que no las
 * documentaré.
 */
class ConfiguracionInterface
{
public:

    ConfiguracionInterface (ConfiguracionPauta *pConf);
    ~ConfiguracionInterface();

    void clear ();
    bool estaContenido (const QString &valor);
    void actualizarNombre (const QString &antiguo,
                           const QString &nuevo);
    int buscarIndice (const QString &value);
    bool estaAceptado (const QString nombre);
    void actualizarCategoria (const QString &elem, const QString &cat, QModelIndex index);
    void actualizarPos(QModelIndex index);
    void appendROI (QString nombre, QPolygon poly);
    QList <QString> inicializarCategorias();
    QString getCatAceptado ();
    inline Categoria getCategoria(QString &nombre){return pConf->getCategoria(nombre);}
    ConfiguracionPauta::roiStruct getRegionOfInterest (QString nombre);
private:
    int cantCategorPersonalizadas;
    ConfiguracionPauta *pConf;

};

#endif // CONFIGURACIONINTERFACE_H
