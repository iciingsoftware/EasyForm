#include "csvparser.h"
#include <QFile>
#include <QDebug>

csvParser::csvParser()
{

}

csvParser::~csvParser()
{
}

QStandardItemModel *csvParser::readCSV(const QString &path)
{
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << file.errorString();
        return new QStandardItemModel;
    }

    QStandardItemModel *model = new QStandardItemModel;
    int row = 0;
    while (!file.atEnd())
    {
        QByteArray line = file.readLine();
        QList <QByteArray> lineList = line.split(',');
        QStandardItem *item = new QStandardItem (QString (lineList.first()));
        QStandardItem *item2 = new QStandardItem (QString (lineList.at(1)));
        model->setItem(row,0, item);
        model->setItem(row,1, item2);
        row++;

    }
    return model;
}

void csvParser::writeCSV(QStandardItemModel *model, const QString &path, QIODevice::OpenMode mode)
{
    QFile file (path);
    if (file.open(mode))
    {
        for (int i = 0; i < model->rowCount(); i ++)
        {
            QStandardItem *col0;
            QStandardItem *col1;
            col0 = model->item(i, 0);
            col1 = model->item(i, 1);
            QTextStream output(&file);
            output << col0->data(Qt::DisplayRole).toString() <<","
                   << col1->data(Qt::DisplayRole).toString();

            if (!col1->data(Qt::DisplayRole).toString().contains("\n"))
                output <<"\n";

        }
    }
    file.close();
}

