#ifndef TREECLASIFICACIONPROXYSTYLE_H
#define TREECLASIFICACIONPROXYSTYLE_H
#include <QProxyStyle>


class TreeClasificacionProxyStyle: public QProxyStyle
{
public:
    TreeClasificacionProxyStyle(QStyle* style = 0);
    ~TreeClasificacionProxyStyle();

    void drawPrimitive ( PrimitiveElement element,
                            const QStyleOption * option,
                            QPainter * painter,
                            const QWidget * widget = 0 ) const;
};

#endif // TREECLASIFICACIONPROXYSTYLE_H
