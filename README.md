# EasyForm #

El proyecto que realizaremos para Ingenería de Software(ya no está en mantención).
En este read me colocaré las caracteristicas que aún están pendientes por ser desarrolladas, asi que por lo tanto me gustaría que por lo menos me ayuden si tampoco es tan dificil lo que queda.

## Documentación ##
la página con la documentación de las clases se encuentra aca:
[Documentación EasyForm](https://dl.dropboxusercontent.com/u/76174164/html/index.html)

## Compilación ##

para compilar el codigo fuente se necesita del Framework Qt que funciona con el compilador Mingw(no la versión para Visual Studio), además necesitan de la libería opencv.
Esta librería se las subo compilada para Windows [acá](https://mega.nz/#!9BcE2IgL!4gZwfyTqgLJ6Sa7yy0bX73HUJzGfKIKOtr19H-Md_ao).
Una vez descomprimida en cualquier lado necesitamos linkearlo, para eso abrimos en archivo EasyForm.pro y editamos las lineas:

```
#!.pro

INCLUDEPATH += C:\\opencv\\build\\x86\\Mingw\\install\\include
```

```
#!.pro

LIBS += -LC:\\opencv\\build\\x86\\Mingw\\install\\x64\\mingw\\lib \

```

por la ruta en la cual se descomprimieron las carpetas "include" y "lib" respectivamente.
Finalmente necesitan agregar a $path la carpeta "bin" o bien colocar los .dll dentro de la carpeta donde se encuentra el .exe compilado.

## Carateristicas aún por desarrollar ##

* El icono
* Mejorar la usabilidad (necesito sugerencias)
* ~~Permitir al usuario, mediante un cuadro de selección, marcar una región de interés que le de mas flexibilidad al programa~~
* Permitir al usuario editar las filas individualmente que el programa considera como respuestas
* procesar las imagenes dentro de uno o varios threads diferentes al de ejecución.
* ~~Reconocer una categoría de identificación para diferenciar los alumnos en el caso de formularios evaluables~~
* ~~Implementar una interfaz para editar una lista de nombres con los que asociar las identificaciones.~~
* Interfaz con la cual editar un texto al cual asignarle al grado de acuerdo a la escala de Likert
* Los formularios no evaluables
* Resaltar con colores las respuestas correctas, las erroneas, las dejadas en blanco y las nulas con colores.
* Las estadisticas por hoja de los formularios no evaluables consistiran en la cantidad de gente que ha respondido cada alternativa de cada pregunta
* Exportar los resultados a formato html, pdf o xlsx
* Generación de una tabla general de datos.

Lista de prioridades

1. Ordenar y limpiar el código
2. Probar y/o terminar la selección personalizada
3. Agregar un tipo de respuesta para que sea número identificador (evaluable)
4. Crear un if y desarrollar formularios no evaluables
   -Cantidad de gente que respondió cada pregunta
5. Tabla general de datos. Si se hace, va aquí. Si no, se salta.
6. Exportar tabla a open office

Usabilidad:
-~~Permitir que el usuario haga cuadros de selección personalizados~~
-Poder importar excel (CSV por ejemplo) para diccionario nombre-rut (evaluable)
-Poder asignar los números likert a textos (desacuerdo, de acuerdo...)
-Cualquier otra cosa