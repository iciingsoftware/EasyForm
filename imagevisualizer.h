#ifndef IMAGEVISUALIZER_H
#define IMAGEVISUALIZER_H
#include <QLabel>
#include <QRubberBand>
#include "treeclasificacionwrappercontroller.h"
#include "configuracionpauta.h"

/**
 * @brief Clase encargada de mostrar las imagenes en pantalla.
 *
 * ImageVisualizer hereda la funcionalidad de Qlablel que ya contiene los
 * metodos para imprimir imagenes, pero ademas agregua funcionalidad que permite
 * hacer zoom (sin limites asi que se puede caer la wea),permite
 * dibujar un cuadro de selección con el mouse y dibuja de un color verde las
 * ROI para OMR.
 */
class ImageVisualizer: public QLabel
{
    Q_OBJECT
public:
    /**
     * @brief Constructor típico de una aplicacion que hereda de QWidget.
     * @param El widget padre, Qt gestiona todo esto.
     */
    ImageVisualizer(QWidget *parent = 0);
    ~ImageVisualizer();

    /**
     * @brief Setea una imagen para mostrarla por pantalla.
     *
     * Además de una imagen tambien requiere de la configuración puesto que
     * esta clase no sólo se limita a dibujar la imagen, sino que tambien dibuja
     * las ROIs y resalta la que está aceptada.
     *
     * @param pixmap Imagen en formato QPixmap.
     * @param conf La configuración de la sesión.
     */
    void setPixmap(const QPixmap &pixmap, ConfiguracionPauta *conf);

    /// Obtiene la imagen.
    QPixmap getPix() const;
    /// Setea sólo la imagen
    void setPix(const QPixmap &value);

    /// Retorna la interfaz de la configuración.
    ConfiguracionInterface *getTreeCategorias() const;
    /// setea la interfaz de la configuración.
    void setTreeCategorias(ConfiguracionInterface *value);

public slots:
    /// @deprecated Setea el indice resaltado.
    void setIndiceResaltado (int indice);
    /// @deprecated Setea los cuadros aceptados. Esto lo realiza desde la configuración
    void setaceptados(int indice);
    /**
     * @brief resalta los cuadros aceptados.
     *
     * Esta función tambien es usada para actualizar lo que sucede cuando un usuario
     * mueve una roi a la categoria aceptado y cuando dibuja una nueva roi en
     * el imageVizualizer.
     */
    void pintarCuadrosAceptados ();
    /**
     * Creo que solo llama a pintar aceptados, pero actualizar era mas intuitivo.
     */
    void actualizar();

signals:
    /**
     * @brief emited: Cuando el usuario crea una nueva ROI.
     *
     * ImageVisualizer soporta la creacion de un cuadro de selección arrastrando el
     * mouse mientra se mantiene el boton izquierdo apretado, el area que marcó se
     * considerará la nueva ROI, cuando eso sucede se dispara esta signal.
     * @param roi
     */
    void usuarioCreoROI (QPolygon roi);

protected:
    /// evento de gestion de la rueda del mouse para el zoom
    void wheelEvent(QWheelEvent *event);
    /**
     * @brief Hace zoom a la imagen
     *
     * Cambia el factor de escala y luego solicita una actualizacion para que
     * este cambio se vea reflejado en la forma de agrandar o disminuir la imagen.
     * @param factorEscala El zoom que se aplicará.
     */
    void escalarImagen(double factorEscala);
    /// Evento de gestion del mouse
    void mousePressEvent(QMouseEvent *e);
    /// Evento de gestion del mouse
    void mouseMoveEvent(QMouseEvent *e);
    /// Evento de gestion del mouse
    void mouseReleaseEvent(QMouseEvent *e);
    /**
     * @brief Crea una nueva roi.
     *
     * Cuando el usuario suelta el click izquierdo se llama a esta función.
     * Entonces se compensa el zoom en la nueva roi y ademas se limita a los
     * bordes de la imagen en caso de que el usuario se haya salido de dicho
     * borde.
     */
    void crearROI ();

    bool pan,debug;
    int panX, panY;
    int indiceResaltado;
    double escalaActual;
    QRubberBand *rubberBand;
    QPoint rubberBandStartPoint;
    /// Se guarda una copia de la imagen de forma de que ésta no se vea afectada por los continuos cambios de tamaño.
    QPixmap pix;
    QList <bool> aceptados;
    ConfiguracionInterface *treeCategorias;
    ConfiguracionPauta *conf;
};

#endif // IMAGEVISUALIZER_H
