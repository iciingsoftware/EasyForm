#include "treeclasificacionproxystyle.h"
#include <QStyleOption>
#include <QPainter>

TreeClasificacionProxyStyle::TreeClasificacionProxyStyle(QStyle *style)
    :QProxyStyle(style)
{

}

TreeClasificacionProxyStyle::~TreeClasificacionProxyStyle()
{

}

void TreeClasificacionProxyStyle::drawPrimitive(QStyle::PrimitiveElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const
{
    if (element == QStyle::PE_IndicatorItemViewItemDrop and
            !option->rect.isNull() and
            option->rect.left() > 20)
    {
        QPoint p1 (option->rect.bottomLeft());
        QPoint p2 (option->rect.bottomRight());
        painter->drawLine(p1,p2);

    }
    else
        QProxyStyle::drawPrimitive(element,option,
                                   painter,widget);

}

