#ifndef OCV_H
#define OCV_H
#include <opencv2/core/core.hpp>
#include <QImage>
#include <QList>
#include <QStandardItemModel>
#include <vector>
using namespace cv;

# define M_PI           3.14159265358979323846

struct FIndex
{
    int indice;
    int size;
};

class OCV
{
public:
    /********************enums*************************/
    enum Tipo_Respuesta
    {
        ALTERNATIVA,
        IDENTIFICACION
    };

    enum Orientacion_Proyeccion
    {
        VERTICAL,
        HORIZONTAL
    };

    /*****************constructores********************/
    OCV(QImage imagen);
    OCV(Mat imagen);
    /*****************destructor************************/
    ~OCV();
    /*****************funciones**************************/
    QImage BuscarRespuestas (QImage imagen, Tipo_Respuesta scope = ALTERNATIVA);
    std::vector<std::vector<Point> > buscarRectangulos ();
    std::vector<std::vector<Point> > getCuadrados();
    QList <int> getRespuestas ();
    QString getIdentificacionAlumno (bool bFormatoRut = true);
    QImage getImDesarrollo (){return cvMatToQImage(imDesarrollo);}
    int getCantidadAlternativas (){return cantidadRespuestas;}

private:
    /*********************variables******************************/
    Mat imagen;
    QList <int> resp;
    QList <int> idResp;
    std::vector<std::vector<Point> > cuadrados;
    Mat imDesarrollo;
    int cantidadRespuestas;

    /*********************funciones******************************/
    void rotate (Mat &src, double angle, Mat &dst);
    inline cv::Mat QImageToCvMat( const QImage &inImage, bool inCloneImageData = true );
    inline QImage  cvMatToQImage( const cv::Mat &inMat );
    Mat aplicarFiltros (Mat imagen, bool applyErode = true);
    double angulo (Point p1, Point p2, Point p0);
    Mat levelFilter (Mat im, int inNegro, int inBlanco, double gamma);
    void setLabel(cv::Mat& im, const std::string label, std::vector<cv::Point>& contour);
    void setLabel(Mat &im, const std::string label, Rect r);
    QVector <int> pixHorizontalProj(Mat im);
    QVector <int> pixVerticalProj (Mat im);
    QList <Rect> segmentacionProjectionBased (Mat im,
                                             Orientacion_Proyeccion orient);

    QList <FIndex> valorAtipico (QList<FIndex> indices);
    void filtradoInterquartil (QList<Rect> &list,
                               Orientacion_Proyeccion orient);

    QList <QList<QRect> > segmentacionColumnas (Mat im, QList <Rect> &cols, QList <Rect> &rows);
    QList <QList<QRect> > segmentarRespuestas (QList<QList<QRect> > &cols, QList <Rect> &rows);
    QList <QList<QRect> > segmentarRespuestas(QList <Rect> &cols, QList <Rect> &rows);

    QList <int> identificarRespuestas (Mat im, QList <QList <QRect> > &respSegmentadas);
    int contarPixeles (Mat im, int umbral);
    void print ( Mat im,QList<Rect> &rois, Scalar color, int coord = -1,
                 bool bSaveInDisk = false);

    void print (Mat im, QList <QRect> &rois, Scalar color,int coord = -1,
                bool bSaveInDisk = false);


    //void rotate(Mat& src, double angle, Mat& dst);
    QRect rect2QRect (Rect r);
    Rect QRect2Rect (QRect r);
};

#endif // IMAGEN_ANALISIS_H
