﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialogorut.h"
#include "csvparser.h"
#include <QPixmap>
#include <QWheelEvent>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <QSplitter>
#include "ocv.h"
#include <vector>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QProgressBar>
#include <QDirIterator>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    model = 0;

/*****************************configuracion Pauta*************************************/
    pConf  = new ConfiguracionPauta;
    pConf->setEvaluable(true);
    treeController = new ConfiguracionInterface(pConf);
    contRoiUsuario = 0;
    rutAlumnos = csvParser::readCSV("Alumnos.csv");
    /*QSplitter *splitter = new QSplitter;
    splitter->addWidget (ui->scrollArea);
    splitter->setOrientation (Qt::Horizontal);
    this->setCentralWidget (splitter);*/


/******************************connects de configuracion******************************/

    connect (ui->buttonTemplate, SIGNAL(pressed()),
             this, SLOT(loadImage()));
    connect (ui->buttonFolder, SIGNAL(pressed()),
             this, SLOT(setFolder()));
    connect (this,SIGNAL(seleccionCambio(int)),
                ui->labelImagen, SLOT(setIndiceResaltado(int)));

    connect (ui->buttonAceptar, SIGNAL(clicked()),
                this, SLOT(aceptarConfiguracion()));
    connect (ui->labelImagen, SIGNAL(usuarioCreoROI(QPolygon)),
             this, SLOT(appendNewROI(QPolygon)));
    connect (ui->actionCargar_Lista_Rut, SIGNAL(triggered(bool)),
             this, SLOT(ImportarCSV()));

/*************************************************************************************/
    //ui->labelImagen->setSizePolicy (QSizePolicy::Ignored, QSizePolicy::Ignored);
    //ui->labelImagen->setScaledContents (true);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadImage()
{
    QString ruta = QFileDialog::getOpenFileName (0,"abrir imagen","",
            "archivo de imagen (*.jpg *.jpeg *.png *.bmp)");
    if (ruta.isNull())
        return;
    imagen.load (ruta);
    ui->lineEditTemplate->setText(ruta);
    OCV cv (imagen);

    QPixmap px2 = px2.fromImage(imagen);//la imagen con los rectangulos resaltados se pasa a pixmap
    QList <QPolygon> polys;
    polys = transformContour2Poly(cv.buscarRectangulos());
    llenarTreeWidget(polys); //se llena el tree widget de la interfaz

    // se transofrma de std vector a qPolygon
    ui->labelImagen->setPixmap (px2,pConf);
    //se muestra la imagen en la interfaz
}

void MainWindow::cambiarSeleccion()
{
}

void MainWindow::onItemChanged(QTreeWidgetItem *item)
{
}

void MainWindow::aceptarConfiguracion()
{
    if (!model)
        return;
    //pConf->aceptados = model->getAceptados(pConf->regionInteres);
    QList <QRect> aceptados = pConf->obtenerAceptados();
    if (aceptados.isEmpty() or
            ui->lineEditFolder->text().isEmpty())
        return;
    QImage im = imagen.copy(aceptados.first());

    pConf->setEvaluable(ui->checkBoxEvaluable->isChecked());

    if (pConf->esEvaluable())
    {
        OCV ocv (im);
        ocv.BuscarRespuestas(im);
        //pConf->respCorrectas = ocv.getRespuestas();
        QList <int> respCorrectas = ocv.getRespuestas();
        pConf->setRespuestasCorrectas(respCorrectas);
    }

    ui->tabWidget->setCurrentIndex(2);
    ui->buttonAceptar->setEnabled(false);
    realizarOMR(ui->lineEditFolder->text());
    ui->buttonAceptar->setEnabled(true);
}

void MainWindow::actualizarNombre(QString viejo, QString nuevo)
{
    /*for (int i=0; i < pConf->regionInteres.size(); i++)
    {
        if (QString::compare(viejo, pConf->regionInteres.at(i).second) == 0)
            pConf->regionInteres[i].second = nuevo;
    }
    */
}

void MainWindow::setFolder()
{
    QString dir = QFileDialog::getExistingDirectory(this, "directorios");

    if (dir.isNull())
        return;
    ui->lineEditFolder->setText(dir);
}

void MainWindow::appendNewROI(QPolygon roi)
{
    contRoiUsuario++;
    QString texto = QString("Creada por el usuario #%1").
                    arg(QString::number(contRoiUsuario));

    QStandardItem *parent = model->item(1); //categoria rechazados
    QStandardItem *child = new QStandardItem (texto);
    parent->appendRow(child);
    //treeController->appendDato(texto, ConfiguracionInterface::rechazado);
    treeController->appendROI(texto,roi);

    ui->labelImagen->actualizar();
}

void MainWindow::onCambioCategoria(ConfiguracionPauta::roiStruct roi)
{
    if (roi.categoria.getTipo() == Categoria::rechazado)
        return;
    QRect roiBoundingRect = roi.roi.boundingRect();
    QImage imageRoi = imagen.copy(roiBoundingRect);

    OCV cv (imagen);

    if (roi.categoria.getTipo() == Categoria::aceptado)
    {
        cv.BuscarRespuestas(imageRoi);
        imageRoi = cv.getImDesarrollo();
        ui->labelDesarrolloAlternativas->setPixmap(QPixmap::fromImage(imageRoi));
    }
    else if (roi.categoria.getTipo() == Categoria::id)
    {
        cv.BuscarRespuestas(imageRoi,OCV::IDENTIFICACION);
        imageRoi = cv.getImDesarrollo();
        ui->LabelDesarrolloIdentificacion->setPixmap(QPixmap::fromImage(imageRoi));
    }

}

void MainWindow::ImportarCSV()
{
    QString path;
    path = QFileDialog::getOpenFileName(this, "Importar evaluados como csv",
                                        QString(), "Archivos csv (*.csv)");

    QStandardItemModel *model;
    model = csvParser::readCSV(path);
    DialogoRut diag(this);

    int res = diag.exec(model, rutAlumnos);
    if (res)
    {

        int rowCount = rutAlumnos->rowCount();
        for (int i = 0; i < model->rowCount(); i++)
        {

            QStandardItem *c0 = new QStandardItem (model->item(i,0)->data(Qt::DisplayRole).toString());
            QStandardItem *c1 = new QStandardItem (model->item(i,1)->data(Qt::DisplayRole).toString());

            QList <QStandardItem*> l1 = rutAlumnos->findItems(c0->data(Qt::DisplayRole).toString());
            QList <QStandardItem*> l2 = rutAlumnos->findItems(c1->data(Qt::DisplayRole).toString());

            if (l1.isEmpty() and l2.isEmpty())
            {
                rutAlumnos->setItem(rowCount + i, 0, c0);
                rutAlumnos->setItem(rowCount + i, 1, c1);
            }
        }
        csvParser::writeCSV(rutAlumnos, "Alumnos.csv",
                            QFile::WriteOnly| QFile::Truncate);

    }
}


void MainWindow::llenarTreeWidget(QList<QPolygon> rect)
{
    //pConf->regionInteres.clear();
    delete pConf;
    pConf = new ConfiguracionPauta();


    if (model)
    {
        model->clear();
        //treeController->clear();
        disconnect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)),
                   ui->labelImagen, SLOT(pintarCuadrosAceptados()));
        disconnect(model, SIGNAL(cambioCategoria(ConfiguracionPauta::roiStruct)),
                   this, SLOT(onCambioCategoria(ConfiguracionPauta::roiStruct)));
        delete model;
    }
    model = new TreeClasificacionModel;
    model->setTreeController(treeController);
    connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)),
            ui->labelImagen, SLOT(pintarCuadrosAceptados()));
    connect(model, SIGNAL(cambioCategoria(ConfiguracionPauta::roiStruct)),
            this, SLOT(onCambioCategoria(ConfiguracionPauta::roiStruct)));
    //connect (model, SIGNAL(cambioNombre(QString,QString)),
    //         this, SLOT(actualizarNombre(QString,QString)));
    /*
    QStandardItem *aceptados = new QStandardItem (QIcon(":/iconos/green ball.png"),
                                                  "aceptados");
    QStandardItem *rechazados = new QStandardItem (QIcon(":/iconos/red ball.png"),
                                                  "rechazados");
    */

    QList <QString> catList = treeController->inicializarCategorias();
    QList <QString> iconList;
    iconList    << ":/iconos/green ball.png" << ":/iconos/red ball.png"
                << ":/iconos/blue ball.png";

    for (int i = 0; i < catList.size(); i++)
    {
        QStandardItem *cat = new QStandardItem (QIcon(iconList.at(i)),
                                                catList.at(i));
        model->setItem(i, cat);
    }
    for (int i = 0; i < rect.size(); i++)
    {
        /*QPair <QPolygon, QString> polyActual;
        polyActual.first = rect.at(i);
        QString nombre = QString ("rectangulo %1").arg(QString::number(i));
        polyActual.second = nombre;
        //pConf->regionInteres.append(polyActual);
        */
        QString nombre = QString ("rectangulo %1").arg(QString::number(i));
        QStandardItem *child = new QStandardItem (nombre);
        //treeController->appendDato(nombre,ConfiguracionInterface::rechazado);
        treeController->appendROI(nombre,rect.at(i));
        model->item(1)->appendRow(child);
        //rechazados->appendRow(child);
    }
    /*
    model->setTreeController(treeController);
    model->setItem(0,aceptados);
    model->setItem(1, rechazados);
    */

    /*
    treeController->agregarCategoria(aceptados->data(Qt::DisplayRole).toString(),
                                        ConfiguracionInterface::aceptado);
    treeController->agregarCategoria(rechazados->data(Qt::DisplayRole).toString(),
                                     ConfiguracionInterface::rechazado);
                                     */
    ui->treeView->setModel(model);
    model->setSelection(ui->treeView->selectionModel());
    ui->treeView->expandAll();
    ui->labelImagen->setTreeCategorias(treeController);
}

void MainWindow::realizarOMR(QString folder)
{
    QStringList list;
    list << "*.jpg"<< "*.jpeg" <<"*.png"<< "*.bmp";
    QDirIterator it(folder,list,QDir::Files, QDirIterator::NoIteratorFlags);
    QDirIterator itCont (folder,list,QDir::Files, QDirIterator::NoIteratorFlags);
    int cantImagenes = 0;
    respuestasOMR.clear();

    while (itCont.hasNext())
    {
        itCont.next();
        cantImagenes ++;
    }

    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(cantImagenes);
    QImage imagen;
    int value = 0;
    QList <float> promedios;
    QList <int> respCorrect;

    if (pConf->esEvaluable())
        respCorrect = pConf->getRespuestasCorrectas();

    QStringList identificacion;
    QRect roiIdentificacion = pConf->obternerRoiIdentificacion();
    int CantAlternativas;

    while (it.hasNext())
    {
        imagen.load(it.next());
        QImage roi = imagen.copy(pConf->obtenerAceptados().first());

        OCV *ocv = new OCV (roi);

        if (!roiIdentificacion.isNull())
        {
            QImage roiId = imagen.copy(roiIdentificacion);
            ocv->BuscarRespuestas(roiId, OCV::IDENTIFICACION);
            identificacion.append( ocv->getIdentificacionAlumno());//stringlist
        }

        //se buscan las respuestas y despues se agregan a una lista que
        //luego se recorre para mostrar por pantalla
        QImage im2 = ocv->BuscarRespuestas(roi);

        QList <int> imresp = ocv->getRespuestas();
        CantAlternativas = ocv->getCantidadAlternativas();

        int cantidad = 0;

        if (pConf->esEvaluable())
        {

            for (int i = 0; i < imresp.size(); i++)
            {
                //if (imresp.at(i) == pConf->respCorrectas.at(i))
                if (imresp.at(i) == respCorrect.at(i))
                    cantidad++;
            }
            float promedio = (float) cantidad / imresp.size();
            promedios.append(promedio);
        }

        ui->labelImagen_2->setPix(QPixmap::fromImage(im2));
        value++;
        ui->progressBar->setValue(value);
        respuestasOMR.append(imresp);
        delete ocv;
    }

    int col = 0;
    int row = 0;
    QStandardItemModel * model = new QStandardItemModel;

    for (col = 0; col < respuestasOMR.size(); col++)
    {
        for (row = 0; row < respuestasOMR.at(col).size(); row++)
        {
            int num = respuestasOMR.at(col).at(row);
            QStandardItem *item = new QStandardItem (QString::number(num + 1));
            model->setItem(row, col, item);
        }
        //si es que se configuro una ROI para el rut se imprime éste en el header
        //de la tabla
        if (!identificacion.isEmpty())
        {
            QString nombre = buscarNombreDeRut(identificacion.
                                               at(col));
            if (!nombre.isEmpty())
            {
                model->setHeaderData(col, Qt::Horizontal,
                                 QVariant(nombre));
            }
            else if (!identificacion.at(col).isEmpty())
            {
                model->setHeaderData(col, Qt::Horizontal,
                                     QVariant(identificacion.at(col)));
            }

        }
    }

    ui->tablaResultados->setModel(model);

    if (pConf->esEvaluable())
    {
        poblarTablaEvaluables(promedios);
        insertarEstadisticaHoja(promedios, identificacion);
    }
    else
        poblarTablaNoEvaluables(model,CantAlternativas);

    ui->tabWidget->setCurrentIndex(3);
}

float MainWindow::calcularPromedioUniverso(QList<float> promedios)
{
    float prom = 0;
    for(int i = 0; i < promedios.size(); i++)
        prom += promedios.at(i);
    return prom / promedios.size();
}

float MainWindow::calcularDesviacionEstandar(QList<float> promedios, float prom)
{
    float promedio = prom / 100;
    float acum = 0;
    for(int i = 0; i < promedios.size(); i++)
        acum += pow (promedios.at(i) - promedio, 2);

    return sqrt(acum / (promedios.size() - 1));
}

QPair<int, int> MainWindow::respuestaMasAcertada()
{
    QPair <int , int> mejorRespuesta;
    mejorRespuesta.first = 0; // respuesta
    mejorRespuesta.second = 0;// cantidad de veces
    int cantPreguntas = respuestasOMR.first().size();
    int temp;

    QList <int> respCorrectas = pConf->getRespuestasCorrectas();
    for (int i = 0; i < cantPreguntas; i++)
    {
        temp = 0;
        for (int j = 0; j < respuestasOMR.size(); j ++)
        {
            int val = respuestasOMR.at(j).at(i);
            if (val == respCorrectas.at(i))
                temp++;
        }
        if (temp > mejorRespuesta.second)
        {
            mejorRespuesta.first = i + 1;
            mejorRespuesta.second = temp;
        }
    }
    return mejorRespuesta;
}

QPair<int, int> MainWindow::respuestaMasErronea()
{
    QPair <int , int> peorRespuesta;
    peorRespuesta.first = 0; // respuesta
    peorRespuesta.second = 0;// cantidad de veces
    int cantPreguntas = respuestasOMR.first().size();
    int temp;
    QList <int> respCorrectas = pConf->getRespuestasCorrectas();
    for (int i = 0; i < cantPreguntas; i++)
    {
        temp = 0;
        for (int j = 0; j < respuestasOMR.size(); j ++)
        {
            int val = respuestasOMR.at(j).at(i);
            if (val != respCorrectas.at(i))
                temp++;
        }
        if (temp > peorRespuesta.second)
        {
            peorRespuesta.first = i + 1;
            peorRespuesta.second = temp;
        }
    }
    return peorRespuesta;
}

void MainWindow::insertarEstadisticaHoja(QList<float> promedios, QStringList listaRut)
{
    QStandardItemModel *model = new QStandardItemModel;
    ui->tablaEstadisticaHoja->setModel(model);
    QStandardItem *item;
    for (int i = 0; i < promedios.size(); i++)
    {
        QString texto = QString::number(promedios.at(i) * 100);
        texto.append("%");
        item = new QStandardItem (texto);
        model->setItem(i,0,item);

        if (!listaRut.isEmpty())
            model->setHeaderData(i, Qt::Vertical, listaRut.at(i));
    }

    model->setHeaderData(0, Qt::Horizontal, QVariant("Promedios Alumnos"));
    ui->tablaEstadisticaHoja->resizeColumnsToContents();
}

QList<QPolygon> MainWindow::transformContour2Poly(std::vector<std::vector<Point> > rect)
{
    QList <QPolygon> lista;
    for (uint i=0; i< rect.size();i++)
    {
        std::vector<cv::Point> contorno = rect[i];
        QPolygon poly (contorno.size());
        for(uint j=0; j< contorno.size();j++)
        {
            Point punto = contorno[j];
            poly.setPoint(j,QPoint(punto.x, punto.y));
        }
        lista.append(poly);
    }
    return lista;
}

void MainWindow::poblarTablaEvaluables(QList<float> promedios)
{
    QStandardItem *item;
    QStandardItemModel *model2 = new QStandardItemModel;
    item = new QStandardItem ("Promedio");
    model2->setItem(0,0,item);
    item = new QStandardItem ("Desviación Estándar");
    model2->setItem(1,0,item);
    item = new QStandardItem ("Respuesta mas contestada");
    model2->setItem(2,0,item);
    item = new QStandardItem ("Respuesta erronea mas conestada");
    model2->setItem(3,0,item);

    int prom = calcularPromedioUniverso(promedios) * 100; // en procentaje
    QString text = QString ("%1%").arg(QString::number(prom));
    item = new QStandardItem (text);
    model2->setItem(0,1,item);
    float desv = calcularDesviacionEstandar(promedios, prom);
    text = QString::number(desv);
    item = new QStandardItem (text);
    model2->setItem(1,1, item);
    QPair <int, int> preg = respuestaMasAcertada();
    item = new QStandardItem (QString::number(preg.first));
    model2->setItem(2,1, item);
    item = new QStandardItem(QString::number(preg.second));
    model2->setItem(2,2, item);
    preg = respuestaMasErronea();
    item = new QStandardItem (QString::number(preg.first));
    model2->setItem(3,1, item);
    item = new QStandardItem (QString::number(preg.second));
    model2->setItem(3, 2, item);

    model2->setHeaderData(0, Qt::Horizontal, QVariant ("Tendencia"));
    model2->setHeaderData(1 , Qt::Horizontal, QVariant ("Valor"));
    model2->setHeaderData(2, Qt::Horizontal, QVariant ("Cantidad"));
    ui->tablaEstadisticas->setModel(model2);
    ui->tablaEstadisticas->resizeColumnsToContents();
}

void MainWindow::poblarTablaNoEvaluables(QStandardItemModel *respuestas, int cantAlternativas)
{

    QStandardItemModel *model = new QStandardItemModel;
    int resp = respuestas->rowCount();
    int vacia;
    int invalida;

     for (int i = 0; i < respuestas->rowCount(); i++)
     {
         QList <int> intAlternativas;
         vacia = 0;
         invalida = 0;

         for (int j = 0; j < cantAlternativas; j++)
             intAlternativas << 0;

         for (int j = 0; j < respuestas->columnCount(); j++)
         {
             int altern = respuestas->item(i, j)->data(Qt::DisplayRole).toInt() - 1;
             if (altern > 0)
                intAlternativas[altern]++;
             else if (altern == -1)
                 vacia++;
             else if (altern == -2)
                 invalida++;
         }

         for (int j = 0; j < cantAlternativas; j++)
         {
             QStandardItem *item = new QStandardItem (
                                    QString::number(intAlternativas.at(j)));

             model->setItem(i , j, item);
         }
         QStandardItem *itemVacia = new QStandardItem(QString::number(vacia));
         QStandardItem *iteminvalida = new QStandardItem(QString::number(invalida));

         model->setItem(i, cantAlternativas , itemVacia );
         model->setItem(i, cantAlternativas + 1, iteminvalida);
     }

     model->setHeaderData(cantAlternativas , Qt::Horizontal, QVariant ("Vacías"));
     model->setHeaderData(cantAlternativas + 1, Qt::Horizontal, QVariant ("Inválidas"));
     ui->tablaEstadisticaHoja->setModel(model);
     ui->groupBox_4->setHidden(true);
     ui->groupBox_5->setTitle("Cantidad de veces seleccionada cada alternativa");
}

QString MainWindow::buscarNombreDeRut(QString rut)
{
    QString str = eliminarFormatoRut(rut);
    QList<QStandardItem*> items = rutAlumnos->findItems(str);
    if (items.size() == 1)
    {
        //str = items.first()->data(Qt::DisplayRole).toString();
        QStandardItem *item = items.first();
        str = rutAlumnos->item(item->row(),1)->data(Qt::DisplayRole).toString();
        return str;
    }
    return rut;
}

QString MainWindow::eliminarFormatoRut(QString rut)
{
    const QChar *c;
    QString ident;

    c = rut.data();
    for (int i = 0; i < rut.size(); i++)
    {
        if (!(c + i)->isPunct())
            ident.append( *(c+i) );
    }
    return ident;
}



